lexer grammar PonyTextLexer;

tokens {
	LPAREN,
	RPAREN,
	STRING,
	SEMENTIC,
	CMDSTART,
	CMDEND,
	ID,
	NUM,
	STRING,
	PARA_SPLITER,
	COMMA,
	PERCENTAGE,
	LBKT,
	RBKT,
	COLON,
	EXCLAMATION,
	ARROW,
	AT,
	BOOLEAN
}

fragment LETTER: [A-Za-z];
fragment DIGIT: [0-9];
fragment NUMBER: '-'? (DIGIT '.' | '.')? DIGIT+;
fragment NEWLINE: '\r'? '\n';
fragment ESC: '\\' [\\@%#];
fragment WS: [ \r\n\t\u000C];

COMMENT: WS* '#' ~[\r\n]+ -> skip;
PARA_SPLITER: NEWLINE+ -> type(PARA_SPLITER);
CMDSTART: '@' -> type(CMDSTART), pushMode(cmd_mode);
PERCENTAGE_DEF: WS* '%' -> skip, popMode;
SEMENTIC: (~[\\@%\r\n] | ESC)+ -> type(SEMENTIC);

mode cmd_mode;
BOOLT: 'true' -> type(BOOLEAN);
BOOLF: 'false' -> type(BOOLEAN);
ID: [A-Za-z_$~][A-Za-z0-9_\-]* -> type(ID);
LPAREN: '(' -> type(LPAREN);
RPAREN: ')' -> type(RPAREN);
NUM: NUMBER -> type(NUM);
COMMA: ',' -> type(COMMA);
LBRACE: '{' -> type(LBRACE);
RBRACE: '}' -> type(RBRACE);
EXCLAMATION: '!' -> type(EXCLAMATION);
SQUOTE: '`' -> more, pushMode(multiString);
LBRACKET: '[' -> type(LBKT);
RBRACKET: ']' -> type(RBKT);
ATSYMBOL: '@' -> type(AT);
COLON: ':' -> type(COLON);
ARROW: '<-' -> type(ARROW);
RARROW: '->' -> type(RARROW);
PERCENTAGE: '%' WS* -> skip, pushMode(DEFAULT_MODE);
WHITESPACE: WS+ -> skip;
SEMICOL_WS: ';' -> type(CMDEND), popMode;

mode multiString;
ESCAPED: '\\`' -> more;
EQUOTE: '`' -> type(STRING), popMode;
STR_TEXT: . -> more;