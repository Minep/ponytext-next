<# 
    .SYNOPSIS
    Provide easy way to manage the ANTLR grammars

    .DESCRIPTION
    Provide easy way to manage the ANTLR grammars.
    For building & testing the PonyText grammar.

    .PARAMETER action
    The action to execute
        - "build" : export the language scheme to a target
        - "test" : test and debug the language scheme
        - "clean" : clean the cache
        - "help" : print this help message

    .PARAMETER lang
    The target language, only used when action=build.
    For the supported target languages, please refer to ANTLR documentation

    .PARAMETER startProd
    The starting production rule, only used when action=test

    .PARAMETER extras
    Extra parameters to feed into ANTLR's grun.
#>


[CmdletBinding()]
param (
    [Parameter(Position=0)]
    [string]
    $action,
    [Parameter()]
    [string]
    $lang = "",
    [Parameter()]
    [string]
    $startProd = "document",
    [Parameter()]
    [string]
    $extras = ""
)

$langName = "PonyText"
$package = "PonyTextNext.Core.pTextLang.Parser"

switch ($action) {
    "build" {
        if ($lang -eq "") {
            Write-Error "!> You must provide a target language."
            exit
        }
        Write-Host "> Building..."
        Invoke-Expression "antlr4 -Dlanguage=$lang -no-listener -visitor -package '$package' -o build/$lang $langName*.g4"
        Write-Host "> Done"
    }
    "test" {
        if (-not (Test-Path -Path "obj")) {
            New-Item -Name "obj" -ItemType "directory"
        }
        Write-Host "> Building.."
        Invoke-Expression "antlr4 -Dlanguage=Java -no-listener -visitor -o obj/ $langName*.g4"

        Set-Location -Path "obj/"
        Invoke-Expression "javac $langName*.java"

        Write-Host "> Start testing environment (Type below and EOF with ^Z)"
        Invoke-Expression "grun $langName $startProd -gui -tree $extras"

        Set-Location -Path ".."
    }
    "clean" {
        Remove-Item -Path "obj" -Recurse 
    }
    "help" {
        Get-Help "$PSCommandPath" -Detailed
    }
    Default {
        Write-Error "!> Must have an action or unknown action. Use 'ptextSyntax.ps1 help' to see help"
    }
}