parser grammar PonyTextParser;
options {
	tokenVocab = PonyTextLexer;
	language = Java;
}

document: (paragraph | assignment | include | PARA_SPLITER)*;

paragraph: paraElement+;

command: CMDSTART (fn | indexer | ID) CMDEND;

paraElement: SEMENTIC | command;

fn: ID LPAREN arg_construct (COMMA arg_construct)* RPAREN
	| ID LPAREN RPAREN;

assignment: CMDSTART ID ARROW arg CMDEND
			| CMDSTART indexer ARROW arg CMDEND;

indexer: ID LBKT primitive RBKT
		| ID LBKT indexer RBKT;

include: CMDSTART AT STRING (RARROW arg)? CMDEND;

arg_construct: (ID COLON)? arg;

arg: primitive | arg_array | document | fn | struturedLiteral | dictionary;

arg_array: LBKT primitive (COMMA primitive)* RBKT;

primitive: ID | STRING| NUM | BOOLEAN;

struturedLiteral: EXCLAMATION ID STRING;

dictionary: LBRACE dictionaryBody RBRACE;

dictionaryBody: ID COLON arg COMMA dictionaryBody
				| ID COLON arg;