﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;
using System.Text;

namespace PonyTextNext.Transformers.Utils.Verbalization
{
    public class ChineseVerbalizer
    {
        private readonly string[] Number = new string[] {
            "零","一","二","三","四","五","六","七","八","九"
        };

        private readonly string[] HUnit = new string[] {
            string.Empty, "十", "百", "千"
        };

        private readonly string[] HHUnit = new string[] {
            string.Empty, "万", "亿", "万亿", "兆亿", "京", "垓"
        };

        private const int GroupLen = 3;

        public string verbalize(decimal value) {
            return verbalizeFloat(value.ToString());
        }

        public string verbalizeOneByOne(decimal value) {
            return verbalizeOneByOne(value.ToString());
        }

        public string verbalizeOneByOne(string value) {
            var sb = new StringBuilder();
            foreach (var item in value) {
                if ('0' <= item && item <= '9') {
                    sb.Append(Number[item - '0']);
                }
                else if (item == '.') {
                    sb.Append("点");
                }
                else {
                    throw new ArgumentException("Invalid numeric string");
                }
            }
            return sb.ToString();
        }

        public string verbalizeFloat(string value) {
            var fracts = value.Split('.');
            var verb = verbalizeInteger(fracts[0]);
            if (fracts.Length > 1) {
                var sb = new StringBuilder();
                for (var i = 0; i < fracts[1].Length; i++) {
                    sb.Append(Number[fracts[1][i] - '0']);
                }
                return $"{verb}点{sb}";
            }
            else {
                return verb;
            }
        }

        public string verbalizeInteger(string value) {
            var sb = new StringBuilder();
            sb.Append(value.StartsWith("-") ? "负" : string.Empty);
            value = value.TrimStart('-', '0');
            if (value.Equals("10")) {
                sb.Append(HUnit[1]);
                return sb.ToString();
            }
            var totalDigit = value.Length - 1;
            var hasEncounteredZero = false;
            for (int i = 0; i < value.Length; i++, totalDigit--) {
                var currentChar = value[i];
                if (currentChar == '0') {
                    hasEncounteredZero = true;
                    continue;
                }
                else if (hasEncounteredZero) {
                    sb.Append(Number[0]);
                    hasEncounteredZero = false;
                }
                var hIndex = totalDigit % HUnit.Length;
                sb.Append(Number[currentChar - '0']);
                sb.Append(HUnit[hIndex]);
                while (i + 1 < value.Length && value[i + 1] == '0' && hIndex != 0) {
                    totalDigit--;
                    i++;
                    hasEncounteredZero = true;
                    hIndex = totalDigit % HUnit.Length;
                }
                if (hIndex == 0) {
                    sb.Append(HHUnit[totalDigit / HUnit.Length]);
                }
            }
            var result = sb.ToString();
            return result.Length == 0 ? Number[0] : result;
        }

        protected string digitPlaceToUnit(int digitPlace) {
            var hIndex = digitPlace % HUnit.Length;
            var hhIndex = (digitPlace - hIndex) / HUnit.Length;
            return $"{HUnit[hIndex]}{HHUnit[hhIndex]}";
        }
    }
}
