﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Exceptions;
using System;

namespace PonyTextNext.Transformers.Utils.Verbalization
{
    public class VerbalizationFormatter : ICustomFormatter, IFormatProvider
    {
        private ChineseVerbalizer chineseVerbalizer;

        public VerbalizationFormatter() {
            chineseVerbalizer = new ChineseVerbalizer();
        }

        public string Format(string format, object arg, IFormatProvider formatProvider) {
            if (arg == null) {
                return string.Empty;
            }
            if (!(arg is int)) {
                return arg.ToString();
            }
            var intArg = (int) arg;
            switch (format) {
                case "ch":
                    return chineseVerbalizer.verbalize(intArg);
                case "CH":
                    return chineseVerbalizer.verbalizeOneByOne(intArg);
                case "num":
                    return intArg.ToString();
                default:
                    throw new PonyTextException($"Unsupport verbalization language of '{format}'");
            }
        }

        public object GetFormat(Type formatType) {
            if (formatType == typeof(ICustomFormatter)) {
                return this;
            }
            return null;
        }
    }
}
