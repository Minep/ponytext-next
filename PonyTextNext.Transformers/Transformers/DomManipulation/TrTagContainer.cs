﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.DomManipulation
{
    [Transformer("$tag-block")]
    public class TrTagContainer : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal)
                .Positional(CallSignature.Any);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var tagName = AcquireArgumentAs<string>(0);
            var body = AcquireArgument(1).EvaluateToDOM(engine);
            var root = new DOMRoot(tagName);
            root.AddToChildren(body);
            return root;
        }
    }
}
