﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.DomManipulation
{
    [Transformer("$html")]
    class TrHtml : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal);
        }
        protected override DOMElement Invoked(IEvaluationEngine engine) {
            if (!engine.Context.AllowHTML) {
                throw new PonyTextException(
                    "PonyText is prohibit the insertion of HTML and Javascript code" +
                    " in prevention of possible XSS attack. This is default behavior, please append " +
                    "appropriate flag to allow it and try again.");
            }
            var html = AcquireArgumentAs<string>(0);
            return new DOMLeaf(html, false);
        }
    }
}
