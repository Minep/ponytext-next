﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.ParagraphLevel;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.DomManipulation
{
    [Transformer("$tag-para")]
    public class TrTagParagraph : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal)
                .Positional(
                    DefaultFragments.StringTerminal,
                    DefaultFragments.Invocation,
                    DefaultFragments.Reference
                );
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var tagName = AcquireArgumentAs<string>(0);
            var bodies = AcquireArgument(1).EvaluateToDOM(engine).FlattenToList();
            var root = new DOMParagraph(tagName);
            root.AddToChildrenRanged(bodies);
            return root;
        }
    }
}
