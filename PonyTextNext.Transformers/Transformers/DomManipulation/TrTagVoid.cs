﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.DomManipulation
{
    [Transformer("$tag-void")]
    public class TrTagVoid : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal)
                .NamedOptional("attrs", DefaultFragments.SLiteral);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var tagName = AcquireArgumentAs<string>(0);
            var attrs = GetArgumentAs<KeyValueProperties>("attrs");

            var tag = new DOMVoid(tagName);
            if (attrs != null) {
                tag.Attributes.OverrideWith(attrs);
            }
            return tag;
        }
    }
}
