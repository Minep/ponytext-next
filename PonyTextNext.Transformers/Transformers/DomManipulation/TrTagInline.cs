﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.ParagraphLevel;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.DomManipulation
{
    [Transformer("$tag-inline")]
    public class TrTagInline : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal)
                .Positional(CallSignature.Any);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var tagName = AcquireArgumentAs<string>(0);
            var bodies = AcquireArgument(1).EvaluateToDOM(engine).FlattenToList();

            // do type check
            foreach (var item in bodies) {
                if (item.Type <= DOMType.Inline) {
                    throw new PonyTextException("Inline node can only contains runs of leaves");
                }
            }

            var root = new DOMInline(tagName);
            root.AddToChildrenRanged(bodies);
            return root;
        }
    }
}
