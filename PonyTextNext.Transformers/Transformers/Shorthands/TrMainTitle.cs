﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;
using PonyTextNext.Api.Utilities;

namespace PonyTextNext.Transformers.Transformers.Shorthands
{
    [Transformer("main-title")]
    class TrMainTitle : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(CallSignature.Any);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var body = AcquireArgument(0).EvaluateToDOM(engine);
            var proot = body.Wrap(HTMLTag.Paragraph);
            proot.StyleClasses.Add("main-title");
            return proot;
        }
    }
}
