﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;
using System;

namespace PonyTextNext.Transformers.Transformers.Shorthands
{
    [Transformer("today")]
    class TrDateToday : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .NamedOptional("fmt", DefaultFragments.StringTerminal);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var format = GetArgumentAs("fmt", "D");
            return DOMLeaf.Create(DateTime.Now.ToString(format));
        }
    }
}
