﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.Document.Fragments;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.ParagraphLevel;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;
using PonyTextNext.Api.Utilities;
using PonyTextNext.Transformers.Utils.Verbalization;
using System.Text;

namespace PonyTextNext.Transformers.Transformers
{
    [Transformer("heading")]
    public class TrHeading : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.NumberTerminal)
                .NamedOptional("text", CallSignature.Any)
                .NamedOptional("counter", DefaultFragments.BooleanTerminal);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var level = AcquireArgumentAs<int>(0) - 1;
            var useCounter = GetArgumentAs<bool>("counter", alternative: true);
            if (level < 0 || level > 5) {
                throw new PonyTextException($"Invalid heading level: {level}. Must between 1 and 6 inclusive.");
            }

            DOMRoot heading = null;
            if (useCounter)
                engine.CountHeadingNumber(level);

            var id = GetHeadingId(level, engine.Context.HeadingCounter);
            engine.Context.HeadingTracker.PutHeading(level, id);

            var body = GetArgumentOrElse("text", DefaultFragments.EmptyFragment).EvaluateToDOM(engine);
            var prefix = GetArgumentOrElse("prefix", DefaultFragments.EmptyFragment).EvaluateToDOM(engine);
            var suffix = GetArgumentOrElse("suffix", DefaultFragments.EmptyFragment).EvaluateToDOM(engine);
            var shape = GetArgumentAs("shape", "display");

            switch (shape) {
                case "display":
                    heading = createDispalyShapeHeading(level, body, suffix, prefix);
                    break;
                case "inline":
                    heading = createInlineShapeHeading(level, body, suffix, prefix);
                    break;
            }
            heading.Attributes.SetProperty("id", id);

            return heading;
        }

        private DOMRoot createDispalyShapeHeading(int level, DOMElement body, DOMElement suffix, DOMElement prefix) {
            var heading = new DOMHeading(level);
            var bodyPart = new DOMInline(HTMLTag.Span);
            var prefixPart = new DOMInline(HTMLTag.Span);
            var suffixPart = new DOMInline(HTMLTag.Span);

            bodyPart.AddToChildrenRanged(body.FlattenToList());
            prefixPart.AddToChildrenRanged(prefix.FlattenToList());
            suffixPart.AddToChildrenRanged(suffix.FlattenToList());

            prefixPart.StyleClasses.Add("h-prefix");
            suffixPart.StyleClasses.Add("h-suffix");
            bodyPart.StyleClasses.Add("h-body");

            heading.AddToChildren(prefixPart);
            heading.AddToChildren(bodyPart);
            heading.AddToChildren(suffixPart);

            heading.StyleClasses.Add("heading-block");

            return heading;
        }

        private DOMRoot createInlineShapeHeading(int level, DOMElement body, DOMElement suffix, DOMElement prefix) {
            var heading = new DOMHeading(level);
            var prefixPart = prefix.FlattenToList();
            var suffixPart = suffix.FlattenToList();
            var bodyPart = body.FlattenToList();
            heading.AddToChildrenRanged(prefixPart);
            heading.AddToChildrenRanged(bodyPart);
            heading.AddToChildrenRanged(suffixPart);
            return heading;
        }

        private string GetHeadingId(int level, HeadingCounter counter) {
            return $"h{string.Join('-', counter.EnumerateToLevel(level))}";
        }
    }
}
