﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.List
{
    [Transformer("item")]
    class TrListItem : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(CallSignature.Any);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var itemContent = AcquireArgument(0).EvaluateToDOM(engine);
            var listItem = new DOMRoot(HTMLTag.ListItem);
            listItem.AddToChildren(itemContent);
            return listItem;
        }
    }
}
