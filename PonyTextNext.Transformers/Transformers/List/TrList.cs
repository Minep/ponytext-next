﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.List
{
    [Transformer("list")]
    public class TrList : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.Container)
                .NamedOptional("ordered", DefaultFragments.StringTerminal);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var listItems = AcquireArgument(0).EvaluateToDOM(engine);
            var ordered = GetArgumentAs("ordered", alternative: false);
            var listRoot = new DOMRoot(ordered ? HTMLTag.ListOrdered : HTMLTag.List);
            foreach (var child in listItems.Children()) {
                var root = child as DOMRoot;
                if (root == null || !root.Tag.Equals(HTMLTag.ListItem)) {
                    continue;
                }
                listRoot.AddToChildren(root);
            }
            return listRoot;
        }
    }
}
