﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.ParagraphLevel;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers
{
    [Transformer("styled")]
    public class TrStyled : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(CallSignature.Any)
                .Positional(DefaultFragments.SLiteral);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var subtree = AcquireArgument(0).EvaluateToDOM(engine);
            var literal = AcquireArgumentAs<KeyValueProperties>(1);
            if (subtree.Type == DOMType.Leaf) {
                var root = new DOMInline(HTMLTag.Span);
                root.AddToChildren(subtree);
                subtree = root;
            }
           (subtree as DOMRoot).InlineStyle.Union(literal);
            return subtree;
        }
    }
}
