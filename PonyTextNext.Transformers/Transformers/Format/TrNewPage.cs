﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.ParagraphLevel;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.Format
{
    [Transformer("new-page")]
    public class TrNewPage : TransformerBase
    {
        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var root = new DOMParagraph("p");
            root.InlineStyle
                .SetProperty("break-before", "page")
                .SetProperty("margin", "0");
            return root;
        }
    }
}
