﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.Format
{
    [Transformer("br")]
    public class TrParagraphBreak : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {

        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            return new DOMVoid(HTMLTag.Break);
        }
    }
}
