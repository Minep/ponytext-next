﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.ParagraphLevel;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;
using System;

namespace PonyTextNext.Transformers.Transformers.Format
{
    [Transformer("align")]
    public class Align : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal)
                .Positional(CallSignature.Any);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var direction = AcquireArgumentAs<string>(0);

            if (direction.Equals("center") || direction.Equals("left") || direction.Equals("right")) {
                var body = AcquireArgument(1).EvaluateToDOM(engine);
                DOMRoot root;
                if (body.Type == DOMType.Root) {
                    root = (DOMRoot) body;
                }
                else {
                    root = new DOMParagraph(HTMLTag.Paragraph);
                    root.AddToChildren(body);
                }
                root.InlineStyle.SetProperty("text-align", direction);
                return root;
            }
            throw new ArgumentException("Alignment direction should be either 'center', 'left' or 'right'.");
        }
    }
}
