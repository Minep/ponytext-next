﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Config;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.Format
{
    [Transformer("page-fmt")]
    public class PageStyler : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.SLiteral)
                .NamedOptional("for-type", DefaultFragments.StringTerminal);
        }
        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var css = AcquireArgumentAs<KeyValueProperties>(0);
            var type = GetArgumentAs("for-type", string.Empty);
            engine.Context.StyleSheet.AtRules.ComputeValue("page", v => {
                if (v == null) {
                    v = new AtRules("page");
                }
                if (!string.IsNullOrEmpty(type)) {
                    v.SetSubRule($"@{type}", css);
                }
                else {
                    v.SetRuleStyle(css);
                }
                return v;
            });
            return DOMElement.None;
        }
    }
}
