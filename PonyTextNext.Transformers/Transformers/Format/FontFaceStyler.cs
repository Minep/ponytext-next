﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.ParagraphLevel;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.Format
{
    [Transformer("b")]
    public class BoldStyler : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature.Positional(CallSignature.Any);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var root = new DOMInline("strong");
            var subtree = AcquireArgument(0).EvaluateToDOM(engine);
            root.AddToChildrenRanged(subtree.FlattenToList());
            return root;
        }
    }

    [Transformer("i")]
    public class ItalicStyler : TransformerBase
    {
        public ItalicStyler() : base() {
        }

        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature.Positional(CallSignature.Any);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var root = new DOMInline("em");
            var subtree = AcquireArgument(0).EvaluateToDOM(engine);
            root.AddToChildrenRanged(subtree.FlattenToList());
            return root;
        }
    }

    [Transformer("u")]
    public class UnderlineStyler : TransformerBase
    {
        public UnderlineStyler() : base() {
        }

        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature.Positional(CallSignature.Any);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var root = new DOMInline("u");
            var subtree = AcquireArgument(0).EvaluateToDOM(engine);
            root.AddToChildrenRanged(subtree.FlattenToList());
            return root;
        }
    }
}
