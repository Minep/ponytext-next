﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;
using PonyTextNext.Transformers.Utils.Verbalization;
using System;

namespace PonyTextNext.Transformers.Transformers
{
    [Transformer("verbal")]
    public class TrReadNum : TransformerBase
    {
        private ChineseVerbalizer chineseVerbalizer;
        public TrReadNum() : base() {
            chineseVerbalizer = new ChineseVerbalizer();
        }

        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.NumberTerminal, DefaultFragments.StringTerminal)
                .NamedOptional("lang", DefaultFragments.StringTerminal)
                .NamedOptional("mode", DefaultFragments.StringTerminal);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var number = AcquireArgumentAs<string>(0);
            var lang = GetArgumentAs<string>("lang", "cn");
            var mode = GetArgumentAs<string>("mode", "whole");
            if (!lang.Equals("cn")) {
                throw new ArgumentException($"Language '{lang}' is not supported");
            }
            if (mode.Equals("whole")) {
                return DOMLeaf.Create(chineseVerbalizer.verbalizeFloat(number));
            }
            else if (mode.Equals("charwise")) {
                return DOMLeaf.Create(chineseVerbalizer.verbalizeOneByOne(number));
            }
            throw new ArgumentException($"Unknown verbalization mode: '{lang}'");
        }
    }
}
