﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.ParagraphLevel;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;
using System.IO;

namespace PonyTextNext.Transformers.Transformers
{
    [Transformer("js")]
    public class TrScript : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal)
                .NamedOptional("external", DefaultFragments.StringTerminal);
        }
        protected override DOMElement Invoked(IEvaluationEngine engine) {
            if (!engine.Context.AllowHTML) {
                throw new PonyTextException(
                    "PonyText is prohibit the insertion of HTML and Javascript code" +
                    " in prevention of possible XSS attack. This is default behavior, please append " +
                    "appropriate flag to allow it and try again.");
            }
            var src = AcquireArgumentAs<string>(0);
            var external = GetArgumentAs("external", false);
            var root = new DOMInline("script");
            root.Attributes.SetProperty("type", "text/javascript");
            if (external) {
                var path = Path.Combine(engine.Context.BasePath, src);
                src = File.ReadAllText(path);
            }
            root.AddToChildren(new DOMLeaf(src, false));
            return root;
        }
    }

    [Transformer("js-remote")]
    public class TrScriptRemote : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal);
        }
        protected override DOMElement Invoked(IEvaluationEngine engine) {
            if (!engine.Context.AllowHTML) {
                throw new PonyTextException(
                    "PonyText is prohibit the insertion of HTML and Javascript code" +
                    " in prevention of possible XSS attack. This is default behavior, please append " +
                    "appropriate flag to allow it and try again.");
            }
            var src = AcquireArgumentAs<string>(0);
            var root = new DOMInline("script");
            root.Attributes.SetProperty("type", "text/javascript");
            root.Attributes.SetProperty("src", src);
            return root;
        }
    }
}
