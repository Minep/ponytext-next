﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;
using System;

namespace PonyTextNext.Transformers.Transformers.Flow
{
    [Transformer("$try")]
    public class TrTryCatch : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(CallSignature.Any)
                .NamedOptional("catch", CallSignature.Any);
        }
        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var @try = AcquireArgument(0);
            var @catch = GetArgumentOrElse("catch", null);
            try {
                return @try.EvaluateToDOM(engine);
            }
            catch (Exception) {
                return @catch?.EvaluateToDOM(engine);
            }
        }
    }
}
