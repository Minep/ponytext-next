﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers.Flow
{
    [Transformer("$if-def")]
    public class TrDefBranching : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal)
                .Positional(CallSignature.Any)
                .NamedOptional("otherwise", CallSignature.Any);
        }
        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var toCheck = AcquireArgumentAs<string>(0);
            var branch = AcquireArgument(1);
            var elseBranch = GetArgumentOrElse("otherwise", null);
            try {
                engine.ResolveReference(toCheck);
                return branch.EvaluateToDOM(engine);
            }
            catch (System.Exception) {
                return elseBranch?.EvaluateToDOM(engine);
            }
        }
    }
}