﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers
{
    [Transformer("css")]
    public class TrCss : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal)
                .Positional(DefaultFragments.SLiteral);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var name = AcquireArgumentAs<string>(0);
            var kvp = AcquireArgumentAs<KeyValueProperties>(1);
            engine.Context.StyleSheet.Classes.SetRegister(name, kvp);
            return DOMElement.None;
        }
    }
}
