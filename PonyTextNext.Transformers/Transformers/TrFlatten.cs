﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers
{
    [Transformer("flat")]
    public class TrFlatten : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(CallSignature.Any)
                .NamedOptional("deep", DefaultFragments.StringTerminal);
        }
 
        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var subtree = AcquireArgument(0).EvaluateToDOM(engine);
            var isDeep = GetArgumentAs("deep", true);
            if (isDeep) {
                var nodeList = subtree.FlattenToList();
                if (nodeList.Count == 0) {
                    return DOMElement.None;
                }

                var virtualRoot = new DOMRoot(string.Empty);
                foreach (var item in nodeList) {
                    virtualRoot.AddToChildren(item);
                }
                return virtualRoot.DirectSuccessor;
            }
            return subtree.DirectSuccessor;
        }
    }
}
