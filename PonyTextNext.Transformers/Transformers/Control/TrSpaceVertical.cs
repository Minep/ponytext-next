﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.Control;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;
using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Transformers.Transformers.Control
{
    [Transformer("vspace")]
    public class TrSpaceVertical : TransformerBase
    {
        public TrSpaceVertical() {
        }

        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature.Positional(DefaultFragments.StringTerminal);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var size = AcquireArgumentAs<string>(0);
            return new DOMSpacing(DOMSpacing.ORIENTATION_VERTICAL, size);
        }
    }
}
