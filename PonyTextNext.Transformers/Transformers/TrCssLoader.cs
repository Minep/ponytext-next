﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;
using System.IO;

namespace PonyTextNext.Transformers.Transformers
{
    [Transformer("css-from")]
    public class TrCssLoader : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal);
        }
        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var path = AcquireArgumentAs<string>(0);
            path = Path.Combine(engine.Context.BasePath, path);
            engine.Context.StyleSheet.DirectCss.Append(File.ReadAllText(path));
            return DOMElement.None;
        }
    }
}
