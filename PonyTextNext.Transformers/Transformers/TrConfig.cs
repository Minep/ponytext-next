﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.Document.Fragments.Terminals;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers
{
    [Transformer("config")]
    public class TrConfig : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(DefaultFragments.StringTerminal)
                .Positional(DefaultFragments.SLiteral);
        }

        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var key = (string) AcquireArgumentAs<string>(0);
            if (key.Equals("document")) {
                var sliteral = (DocumentStructuredLiteral) AcquireArgumentAsFragment(1, DefaultFragments.SLiteral);
                engine.SLiteralEvaluator.PopulateToSturcture(sliteral, engine.Context.DocumentConfig);
            }
            else {
                var category = key.Split('.');
                if (category.Length < 2) {
                    throw new PonyTextException($"Invalid category name: '{key}'");
                }
                var kvpair = (DocumentDictionary) AcquireArgumentAsFragment(1, DefaultFragments.Dictionary);
                switch (category[0]) {
                    case "transformer":
                        var name = category[1];
                        engine.Context.TransformerOptions.SetRegister(name, kvpair);
                        break;
                    default:
                        throw new PonyTextException($"Unknown category: '{category[0]}'");
                }
            }
            return DOMElement.None;
        }
    }
}
