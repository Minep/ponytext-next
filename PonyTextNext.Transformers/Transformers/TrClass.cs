﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.ParagraphLevel;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;

namespace PonyTextNext.Transformers.Transformers
{
    [Transformer("class")]
    public class TrClass : TransformerBase
    {
        protected override void DefineSignature(CallSignature parameterSignature) {
            parameterSignature
                .Positional(CallSignature.Any)
                .Positional(DefaultFragments.Array);
        }
        protected override DOMElement Invoked(IEvaluationEngine engine) {
            var body = AcquireArgument(0).EvaluateToDOM(engine);
            var clazzArr = AcquireArgumentAs<string[]>(1);
            if (body.Type != DOMType.Root) {
                var span = new DOMInline(HTMLTag.Span);
                span.AddToChildren(body);
                body = span;
            }
            var bodyRoot = (DOMRoot) body;
            bodyRoot.StyleClasses.AddRange(clazzArr);
            return bodyRoot;
        }
    }
}
