﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Runtime.Transformer;
using PonyTextNext.Api.Utilities;
using System;
using System.Collections.Generic;

namespace PonyTextNext.Transformers
{
    public class TransformerLibrary : ITrasnformerLibrary
    {
        public IEnumerable<Type> GetAllTransformers() {
            return GetType().GetAsmTypesWithAttribute<TransformerAttribute>();
        }
    }
}
