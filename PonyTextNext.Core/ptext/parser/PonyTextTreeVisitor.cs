﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using PonyTextNext.Api.Document;
using PonyTextNext.Api.Document.Fragments;
using PonyTextNext.Api.Document.Fragments.Functions;
using PonyTextNext.Api.Document.Fragments.Terminals;
using PonyTextNext.Api.Runtime;
using System.Text.RegularExpressions;

namespace PonyTextNext.Core.pTextLang.Parser
{
    public class PonyTextTreeVisitor : PonyTextParserBaseVisitor<DocumentFragmentBase>
    {
        private static readonly Regex semanticSantizier = new Regex(@"\\([\\@%])", RegexOptions.Compiled);
        private IPonyTextDependencyResolver resolver;

        public PonyTextTreeVisitor(IPonyTextDependencyResolver resolver) {
            this.resolver = resolver;
        }

        public override DocumentFragmentBase VisitDocument([NotNull] PonyTextParser.DocumentContext context) {
            DocumentBlock block = new DocumentBlock(DefaultFragments.Container);

            for (int i = 0; i < context.ChildCount; i++) {
                block.AddToChildren(Visit(context.GetChild(i)));
            }

            return block;
        }

        public override DocumentFragmentBase VisitParagraph([NotNull] PonyTextParser.ParagraphContext context) {
            DocumentParagraph paragraph = new DocumentParagraph();
            foreach (var subtree in context.paraElement()) {
                paragraph.AddToChildren(Visit(subtree));
            }
            return paragraph;
        }

        public override DocumentFragmentBase VisitParaElement([NotNull] PonyTextParser.ParaElementContext context) {
            if (context.SEMENTIC() != null) {
                var text = semanticSantizier.Replace(context.SEMENTIC().GetText(), "$1").TrimStart();
                if (text.Length == 0) {
                    return null;
                }
                return new DocumentTerminal(text, DefaultFragments.StringTerminal, Token.Empty);
            }
            return Visit(context.command());
        }

        public override DocumentFragmentBase VisitCommand([NotNull] PonyTextParser.CommandContext context) {
            if (context.ID() != null) {
                return forVariable(context.ID());
            }
            else if (context.indexer() != null) {
                return Visit(context.indexer());
            }
            else {
                return Visit(context.fn());
            }
        }

        public override DocumentFragmentBase VisitStruturedLiteral([NotNull] PonyTextParser.StruturedLiteralContext context) {
            var structName = context.ID().GetText();
            var data = context.STRING().GetText();
            return new DocumentStructuredLiteral(structName, data.Trim('`'));
        }

        public override DocumentFragmentBase VisitFn([NotNull] PonyTextParser.FnContext context) {
            var fnName = context.ID().GetText();
            DocumentFunctionCall fn = new DocumentFunctionCall(fnName, ToToken(context.ID()));
            foreach (var arg in context.arg_construct()) {
                fn.AddToChildren(VisitArg_construct(arg));
            }
            return fn;
        }

        public override DocumentFragmentBase VisitInclude([NotNull] PonyTextParser.IncludeContext context) {
            var file = context.STRING().GetText().Trim('`');
            DocumentInjectionPoint head = null;
            foreach (var dep in resolver.ResolveDependencies(file)) {
                var injection = new DocumentInjectionPoint(dep, ToToken(context.STRING()));
                if (head != null) {
                    head.AddToSiblings(injection);
                }
                else {
                    head = injection;
                }
            }
            return head;
        }

        public override DocumentFragmentBase VisitIndexer([NotNull] PonyTextParser.IndexerContext context) {
            var id = context.ID();
            var indexer = context.indexer();
            if (indexer == null) {
                return new DocumentIndexer(id.GetText(), Visit(context.primitive()), ToToken(id));
            }
            return new DocumentIndexer(id.GetText(), Visit(indexer), ToToken(id));
        }

        public override DocumentFragmentBase VisitAssignment([NotNull] PonyTextParser.AssignmentContext context) {
            DocumentFragmentBase varName;
            if (context.ID() != null) {
                varName = forVariable(context.ID());
            }
            else {
                varName = Visit(context.indexer());
            }

            var value = VisitArg(context.arg());
            return new DocumentVariableAssignment(varName, value);
        }

        public override DocumentFragmentBase VisitArg_construct([NotNull] PonyTextParser.Arg_constructContext context) {
            var anchor = (context.ID()?.GetText()) ?? "";
            var arg = VisitArg(context.arg());
            return new DocumentFunctionArgument(arg, anchor);
        }

        public override DocumentFragmentBase VisitArg_array([NotNull] PonyTextParser.Arg_arrayContext context) {
            var array = new DocumentTypeArray();
            foreach (var subtree in context.primitive()) {
                array.AddToChildren(VisitPrimitive(subtree));
            }
            return array;
        }

        public override DocumentFragmentBase VisitDictionary([NotNull] PonyTextParser.DictionaryContext context) {
            return Visit(context.dictionaryBody());
        }

        public override DocumentFragmentBase VisitDictionaryBody([NotNull] PonyTextParser.DictionaryBodyContext context) {
            var name = context.ID();
            var value = Visit(context.arg());
            DocumentDictionary dict = null;
            if (context.dictionaryBody() == null) {
                dict = new DocumentDictionary(ToToken(name));   
            }
            else {
                dict = (DocumentDictionary) Visit(context.dictionaryBody());
            }
            dict.AppendFields(name.GetText(), value);
            return dict;
        }

        public override DocumentFragmentBase VisitPrimitive([NotNull] PonyTextParser.PrimitiveContext context) {
            if (context.ID() != null) {
                return forVariable(context.ID());
            }
            else if (context.STRING() != null) {
                string text = context.STRING().GetText().Trim('`').Replace("\\`", "`");
                return new DocumentTerminal(text, DefaultFragments.StringTerminal, ToToken(context.STRING()));
            }
            else if (context.BOOLEAN() != null) {
                string text = context.BOOLEAN().GetText();
                return new DocumentTerminal(text, DefaultFragments.BooleanTerminal, ToToken(context.BOOLEAN()));
            }
            else {
                string text = context.NUM().GetText();
                return new DocumentTerminal(text, DefaultFragments.NumberTerminal, ToToken(context.NUM()));
            }
        }

        private DocumentFragmentBase forVariable(ITerminalNode varNode) {
            return new DocumentMacro(varNode.GetText(), ToToken(varNode));
        }

        private Token ToToken(ITerminalNode terminal) {
            return new Token(terminal.Symbol.Text, terminal.Symbol.Line, terminal.Symbol.Column);
        }
    }
}
