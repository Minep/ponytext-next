﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using Antlr4.Runtime;
using PonyTextNext.Api.Exceptions;

namespace PonyTextNext.Core.pTextLang.Parser
{
    class MyBailErrorStrategy : BailErrorStrategy
    {
        public override void Recover(Antlr4.Runtime.Parser recognizer, RecognitionException e) {
            for (ParserRuleContext context = recognizer.Context; context != null; context = ((ParserRuleContext) context.Parent)) {
                context.exception = e;
            }
            var tkn = e.OffendingToken;
            throw new PonyTextException($"Invalid token: '{tkn.Text}' at line: {tkn.Line}:{tkn.Column}", e);
        }

        public override IToken RecoverInline(Antlr4.Runtime.Parser recognizer) {
            InputMismatchException e = new InputMismatchException(recognizer);
            for (ParserRuleContext context = recognizer.Context; context != null; context = ((ParserRuleContext) context.Parent)) {
                context.exception = e;
            }
            var tkn = e.OffendingToken;
            throw new PonyTextException($"Invalid token: '{tkn.Text}' at line: {tkn.Line}:{tkn.Column}", e);
        }
    }
}
