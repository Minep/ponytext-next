﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using Antlr4.Runtime;
using Serilog;
using System.IO;

namespace PonyTextNext.Core.pTextLang.Parser
{
    class LoggerErrorListener<Symbol> : IAntlrErrorListener<Symbol>
    {
        public void SyntaxError(TextWriter output, IRecognizer recognizer, Symbol offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e) {
            var token = e?.OffendingToken?.Text ?? "<UNKNOWN>";
            Log.Warning($"Invalid token: '{token}' at line {line}:{charPositionInLine}. \n\t More Info: {msg}");
        }
    }
}
