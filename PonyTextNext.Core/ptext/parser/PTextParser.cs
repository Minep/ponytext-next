﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using Antlr4.Runtime;
using PonyTextNext.Api.Document;
using PonyTextNext.Api.Runtime;

namespace PonyTextNext.Core.pTextLang.Parser
{
    public class PTextParser
    {
        private IPonyTextDependencyResolver resolver;

        public PTextParser(IPonyTextDependencyResolver resolver) {
            this.resolver = resolver;
        }

        private DocumentFragmentBase parse(ICharStream charStream) {
            PonyTextTreeVisitor visitor = new PonyTextTreeVisitor(resolver);
            Lexer lexer = new PonyTextLexer(charStream);
            ITokenStream tokenStream = new CommonTokenStream(lexer);
            PonyTextParser parser = new PonyTextParser(tokenStream);
            parser.ErrorHandler = new MyBailErrorStrategy();

            parser.RemoveErrorListeners();
            lexer.RemoveErrorListeners();

            parser.AddErrorListener(new LoggerErrorListener<IToken>());
            lexer.AddErrorListener(new LoggerErrorListener<int>());

            return visitor.Visit(parser.document());
        }

        public DocumentFragmentBase GetVirtualDOM(string filePath) {
            return parse(CharStreams.fromPath(filePath));
        }
    }
}
