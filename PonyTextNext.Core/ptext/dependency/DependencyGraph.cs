﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Utilities;
using System;
using System.Collections.Generic;

namespace PonyTextNext.Core.ptext.dependency
{
    public class DependencyGraph
    {
        private DirectedGraph<string> dependencyRelation;
        private Dictionary<string, DocumentFragmentBase> indexedVDoms;
        private string entryPoint = null;

        public DependencyGraph() {
            dependencyRelation = new DirectedGraph<string>();
            indexedVDoms = new Dictionary<string, DocumentFragmentBase>();
        }

        public void Index(string path, DocumentFragmentBase fragmentBase) {
            indexedVDoms.TryAdd(path, fragmentBase);
        }

        public void SetEntryPoint(string path) {
            entryPoint = path;
        }

        public DocumentFragmentBase GetVertex(string index) {
            return indexedVDoms[index];
        }

        public DocumentFragmentBase EntryPoint => indexedVDoms[entryPoint];

        public void SetRelation(string src, string dependOn) {
            dependencyRelation.PutArrow(src, dependOn);
        }

        public void Verify() {
            if (string.IsNullOrEmpty(entryPoint)) {
                throw new ArgumentNullException("Entrypoint must be present (not null or empty).");
            }
            var cycPath = dependencyRelation.GetCyclicPathFrom(entryPoint);
            if (cycPath.Count > 0) {
                throw new CyclicDependencyException(cycPath);
            }
        }

        public override string ToString() {
            if (string.IsNullOrEmpty(entryPoint)) {
                return string.Empty;
            }
            return dependencyRelation.ToGraphvizString(entryPoint);
        }
    }
}
