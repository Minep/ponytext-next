﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Core.pTextLang.Parser;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PonyTextNext.Core.ptext.dependency
{
    public class PonyTextResolver : IPonyTextDependencyResolver
    {
        private Queue<string> queuedSource;
        /// <summary>
        /// All source files which had seen by resolver so far, queued or not.
        /// </summary>
        private HashSet<string> scannedSource;
        private PTextParser parser;

        public DependencyGraph Graph { get; private set; }

        private readonly string basePath;
        private string currentSource;
        private string currentPathContext;

        public PonyTextResolver(PonyTextContext setting, DependencyGraph dependency) {
            queuedSource = new Queue<string>();
            scannedSource = new HashSet<string>();
            this.parser = new PTextParser(this);
            Graph = dependency;
            this.basePath = setting.BasePath;
            currentPathContext = setting.BasePath;
        }

        public void PushSource(string srcPath) {
            if (scannedSource.Contains(srcPath)) {
                return;
            }
            scannedSource.Add(srcPath);
            queuedSource.Enqueue(srcPath);
        }

        public void Resolve(string entryFile) {
            entryFile = ResolvePaths(entryFile).ToList()[0];
            PushSource(entryFile);
            Graph.SetEntryPoint(entryFile);

            try {
                while (queuedSource.Count > 0) {
                    currentSource = queuedSource.Dequeue();
                    var path = Path.GetFullPath(Path.Combine(basePath, currentSource));
                    currentPathContext = Path.GetDirectoryName(path);

                    var vdom = parser.GetVirtualDOM(path);

                    Graph.Index(currentSource, vdom);
                    Log.Logger.Information("Resolved {0}", currentSource);
                }
                Graph.Verify();
            }
            catch (Exception e) {
                throw new PonyTextException($"When resolving: {currentSource}", e);
            }
            Log.Logger.Information("Total resolved: {0} files", scannedSource.Count);
        }

        public IEnumerable<string> ResolveDependencies(string dependedSource) {
            foreach (var relativeDepPath in ResolvePaths(dependedSource)) {
                PushSource(relativeDepPath);
                Graph.SetRelation(currentSource, relativeDepPath);

                yield return relativeDepPath;
            }
        }

        /// <summary>
        /// Evaluate the src path and 
        /// </summary>
        /// <param name="srcPath"></param>
        /// <returns></returns>
        private IEnumerable<string> ResolvePaths(string srcPath) {
            if (Path.IsPathRooted(srcPath)) {
                // It actually technically viable, I just feel too lazy on working on it.
                throw new PonyTextException($"PonyText does not support importing using absolute path like {srcPath}.");
            }
            var path = Path.Combine(currentPathContext, srcPath);
            path = Path.GetFullPath(path);
            var indexFile = Path.Combine(path, "index.pony");

            if (File.Exists(path)) {
                yield return Path.GetRelativePath(basePath, path);
            }
            else if (File.Exists(indexFile)) {
                yield return Path.GetRelativePath(basePath, indexFile);
            }
            else if (Directory.Exists(path)) {
                foreach (var file in Directory.GetFiles(path, "*.pony")) {
                    yield return Path.GetRelativePath(basePath, file);
                }
            }
            else {
                throw new PonyTextException($"Unable to resolve {srcPath}, not found");
            }
        }

        public string GetDependencyGraph() {
            return Graph.ToString();
        }
    }
}
