﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using Newtonsoft.Json;
using PonyTextNext.Api.Document.Fragments.Terminals;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using System.Collections.Generic;

namespace PonyTextNext.Core.Runtime
{
    public class JsonSLiteralEvaluator : ISLiteralEvaluator
    {
        public T EvaluateAs<T>(DocumentStructuredLiteral structuredLiteral) {
            switch (structuredLiteral.TargetStructure) {
                case "css":
                case "prop":
                    if (typeof(KeyValueProperties).IsAssignableFrom(typeof(T))) {
                        return (T) AsKeyValueProp(structuredLiteral);
                    }
                    break;
                default:
                    break;
            }
            throw new PonyTextException($"SLiteral target type: '{structuredLiteral.TargetStructure}' is not supported.");
        }

        public void PopulateToSturcture(DocumentStructuredLiteral structuredLiteral, object structInstance) {
            if (structuredLiteral.TargetStructure.Equals("prop")) {
                JsonConvert.PopulateObject(structuredLiteral.JsonData, structInstance);
                return;
            }
            throw new PonyTextException($"Can not populate struct with type '{structuredLiteral.TargetStructure}'");
        }

        private object AsKeyValueProp(DocumentStructuredLiteral structuredLiteral) {
            var kvpair = JsonConvert.DeserializeObject<Dictionary<string, string>>(structuredLiteral.JsonData);
            return new KeyValueProperties(kvpair);
        }
    }
}
