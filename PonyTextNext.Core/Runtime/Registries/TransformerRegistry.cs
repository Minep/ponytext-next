﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Registry;
using PonyTextNext.Api.Runtime.Transformer;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace PonyTextNext.Core.Runtime.Registries
{
    public class TransformerRegistry : ITransformerRegistry
    {
        private Dictionary<string, Type> transformers;

        public TransformerRegistry() {
            transformers = new Dictionary<string, Type>();
        }

        public IEnumerable<string> GetRegisteredTransformers() {
            return transformers.Keys;
        }

        public TransformerBase GetTransformerInstance(CallFrame callFrame) {
            try {
                var type = transformers[callFrame.Target];
                var val = (TransformerBase) Activator.CreateInstance(type);
                var name = typeof(TransformerBase).GetField("transformerName", BindingFlags.NonPublic | BindingFlags.Instance);
                name.SetValue(val, callFrame.Target);

                return val;
            }
            catch (Exception e) {
                throw new TransformerException("", e.Message, e.InnerException);
            }
        }

        public bool HasTransformerNamed(string name) {
            return transformers.ContainsKey(name);
        }

        public void LoadFromLibrary<T>() where T : ITrasnformerLibrary {
            var lib = (ITrasnformerLibrary) Activator.CreateInstance(typeof(T));
            RegisterTransformers(lib.GetAllTransformers());
        }

        public void RegisterTransformer<T>() where T : TransformerBase {
            RegisterTransformer(typeof(T));
        }

        public void RegisterTransformer(Type transformerType) {
            var attrs = transformerType.GetCustomAttributes(typeof(TransformerAttribute), false);
            if (attrs.Length == 0) {
                return;
            }
            var transformer = (TransformerAttribute) attrs[0];
            transformers.Add(transformer.Name, transformerType);
        }

        public void RegisterTransformers(IEnumerable<Type> transformers) {
            foreach (var type in transformers) {
                RegisterTransformer(type);
            }
        }
    }
}
