﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api;
using PonyTextNext.Api.Document;
using PonyTextNext.Api.Document.Fragments;
using PonyTextNext.Api.Document.Fragments.Terminals;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Registry;
using PonyTextNext.Api.Utilities;
using PonyTextNext.Core.ptext.dependency;

namespace PonyTextNext.Core.Runtime
{
    public class EvaluationEngine : IEvaluationEngine
    {
        private readonly IMixinInvocator _mixinInvocator;
        private readonly DependencyGraph _dependencies;
        private readonly RegistryBase<DocumentFragmentBase> _symbols;
        private readonly ITransformerRegistry _transformers;
        private readonly PonyTextContext _context;
        private readonly ISLiteralEvaluator _sLiteralEvaluator;

        public EvaluationEngine(
            IMixinInvocator mixinInvocator,
            DependencyGraph dependencies,
            RegistryBase<DocumentFragmentBase> symbols,
            ITransformerRegistry transformers,
            PonyTextContext context,
            ISLiteralEvaluator sLiteralEvaluator
        ) {
            _mixinInvocator = mixinInvocator;
            _dependencies = dependencies;
            _symbols = symbols;
            _transformers = transformers;
            _context = context;
            _sLiteralEvaluator = sLiteralEvaluator;

            UpdateHeadingNumberVariable(context.HeadingCounter);
        }

        public PonyTextContext Context => _context;

        public ISLiteralEvaluator SLiteralEvaluator => _sLiteralEvaluator;

        public RegistryBase<DocumentFragmentBase> SymbolTable => _symbols;

        public T EvaluateSLiteralAs<T>(DocumentStructuredLiteral structuredLiteral) {
            throw new System.NotImplementedException();
        }

        public DOMElement Import(string sourceName) {
            return _dependencies.GetVertex(sourceName).EvaluateToDOM(this);
        }

        public DOMElement Invoke(CallFrame callFrame) {
            if (_transformers.HasTransformerNamed(callFrame.Target)) {
                var instance = _transformers.GetTransformerInstance(callFrame);
                return instance.Transform(this, callFrame);
            }
            return _mixinInvocator.InvokeMixin(this, callFrame);
        }

        public DOMElement Resolve(string variableName) {
            return ResolveReference(variableName).EvaluateToDOM(this);
        }

        public DocumentFragmentBase ResolveReference(string symbolName) {
            if (symbolName.StartsWith("$")) {
                return _mixinInvocator
                        .ResolveMixinParam(symbolName);
            }
            else {
                return _symbols.ReadRegisterSafe(symbolName);
            }
        }

        public void SetValue(string variableName, DocumentFragmentBase varValue) {
            _symbols.SetRegister(variableName, varValue);
        }

        public void CountHeadingNumber(int level) {
            _context.HeadingCounter.Count(level);
            UpdateHeadingNumberVariable(Context.HeadingCounter);
        }

        private void UpdateHeadingNumberVariable(HeadingCounter counter) {
            for (int i = 0; i < counter.Counters.Length; i++) {
                SetValue($"~hL{i + 1}", DocumentTerminal.OfString(counter[i].ToString()));
            }
        }
    }
}
