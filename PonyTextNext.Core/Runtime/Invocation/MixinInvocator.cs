﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Registry;
using System;
using System.Collections.Generic;

namespace PonyTextNext.Core.Runtime.Invocation
{
    public class MixinInvocator : IMixinInvocator
    {
        // $, like $1
        private const int ARG_PREFIX = 1;
        private const int MAX_RECURSION_DEPTH = 128;
        private Stack<CallFrame> stack;
        private RegistryBase<DocumentFragmentBase> varTable;

        public MixinInvocator(RegistryBase<DocumentFragmentBase> varTable) {
            stack = new Stack<CallFrame>();
            this.varTable = varTable;
        }

        public DocumentFragmentBase ResolveMixinParam(string paramName) {
            if (stack.Count == 0) {
                throw new PonyTextException($"Invalid reference of mixin parameter variable: '{paramName}'");
            }
            var currentFrame = stack.Peek();
            return currentFrame.GetArgument(paramName.Substring(ARG_PREFIX));
        }

        DOMElement IMixinInvocator.InvokeMixin(IEvaluationEngine engine, CallFrame callFrame) {
            try {
                if (stack.Count == MAX_RECURSION_DEPTH) {
                    throw new StackOverflowException($"The nested mixin call reach the maximum call depth of {MAX_RECURSION_DEPTH}");
                }

                stack.Push(callFrame);
                var mixin = varTable.ReadRegisterSafe(callFrame.Target).EvaluateToDOM(engine);
                stack.Pop();
                return mixin;
            }
            catch (Exception e) {
                throw new PonyTextException($"Failed to evaluate mixin: '{callFrame.Target}'", e);
            }
        }
    }
}
