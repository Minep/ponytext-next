﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api;
using PonyTextNext.Api.Document.Fragments;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Renderer;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Registry;
using PonyTextNext.Core.ptext.dependency;
using PonyTextNext.Renderer;
using PonyTextNext.Transformers;
using Serilog;
using System.Collections.Generic;

namespace PonyTextNext.Core
{
    public class PonyText
    {
        public IEvaluationEngine Engine { get; }
        public PonyTextContext Setting { get; }
        public PonyTextResolver Resolver { get; }
        public IRendererDispatcher RendererDispatcher { get; }
        public ITransformerRegistry TransformerRegistry { get; }

        public DOMElement Document { get; private set; }

        public PonyText(
            IEvaluationEngine engine,
            PonyTextContext setting,
            PonyTextResolver resolver,
            IRendererDispatcher rendererDispatcher,
            ITransformerRegistry transformerRegistry
        ) {
            Engine = engine;
            Setting = setting;
            Resolver = resolver;
            RendererDispatcher = rendererDispatcher;
            TransformerRegistry = transformerRegistry;

            RendererDispatcher.LoadFromLibrary<RendererLibrary>();
            TransformerRegistry.LoadFromLibrary<TransformerLibrary>();
        }

        public void InjectMacros (IEnumerable<string> macros) {
            if (macros == null) {
                return;
            }
            foreach (var item in macros) {
                string[] kvalue = item.Split('=');
                if (kvalue.Length != 2) {
                    Log.Warning("Invalid macro pair: \"{0}\"", item);
                }
                else {
                    Engine.SetValue(kvalue[0], DocumentTerminal.OfString(kvalue[1]));
                }
            }
        }

        public void Load(string entryFile) {
            try {
                Resolver.Resolve(entryFile);
                Document = Resolver.Graph.EntryPoint.EvaluateToDOM(Engine);
            }
            catch (System.Exception e) {
                throw new PonyTextException($"Fail to load file: '{entryFile}'", e);
            }
        }

        public void RenderToFile(string renderer, string file) {
            RendererDispatcher.RenderToFile(renderer, Document, file);
        }
    }
}
