﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Renderer;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;

namespace PonyTextNext.Core.Renderer
{
    public class RendererDispatcher : IRendererDispatcher
    {
        private Dictionary<string, Type> rendererList;
        private PonyTextContext ponyTextSetting;
        public RendererDispatcher(PonyTextContext ponyTextSetting) {
            this.ponyTextSetting = ponyTextSetting;
            rendererList = new Dictionary<string, Type>();
        }

        public void LoadFromLibrary<T>() where T : IRendererLibrary {
            var instance = (IRendererLibrary) Activator.CreateInstance(typeof(T));
            foreach (var type in instance.GetAllRenderer()) {
                var attrs = type.GetCustomAttributes(typeof(RendererAttribute), false);
                var attr = (RendererAttribute) attrs[0];
                rendererList.Add(attr.Name, type);
            }
        }

        public void RegisterRenderer(string rendererName, Type renderer) {
            if (!typeof(RendererBase).IsAssignableFrom(renderer)) {
                throw new PonyTextException($"'{renderer.Name}' is not a valid renderer.");
            }
            rendererList.Add(rendererName, renderer);
        }

        public void RenderToFile(string renderer, DOMElement element, string file) {
            var path = Path.Combine(ponyTextSetting.BasePath, file);
            using (FileStream sw = new FileStream(path, FileMode.Create)) {
                using (MemoryStream ms = render(renderer, element)) {
                    ms.CopyTo(sw);
                }
            }
        }

        public void RenderToStream(string renderer, DOMElement element, Stream stream) {
            using (MemoryStream ms = render(renderer, element)) {
                ms.CopyTo(stream);
            }
        }

        private MemoryStream render(string renderer, DOMElement element) {
            Type rendererType;
            if (!rendererList.TryGetValue(renderer, out rendererType)) {
                throw new PonyTextException($"Unknown renderer: '{renderer}'");
            }
            RendererBase myRenderer =
                (RendererBase) Activator.CreateInstance(rendererType, element, ponyTextSetting);
            var result = myRenderer.Render();
            Log.Information("Completed");
            Log.Information(myRenderer.Statistic.ToString());
            return result;
        }
    }
}
