﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.Control;
using PonyTextNext.Api.DOM.ParagraphLevel;
using PonyTextNext.Api.Renderer;
using System.Text;

namespace PonyTextNext.Renderer.HTML
{
    public class HTMLVisitor : DOMVisitorBase
    {
        StringBuilder htmlBodyBuilder;
        public DocumentStatistic Statistic { get; }

        public HTMLVisitor() {
            htmlBodyBuilder = new StringBuilder();
            Statistic = new DocumentStatistic();
        }

        public string Body => htmlBodyBuilder.ToString();

        public override void visitInline(DOMInline inlineElement) {
            VisitRootElement(inlineElement);
        }

        public override void visitLeaf(DOMLeaf leafElement) {
            var content = leafElement.Content;
            Statistic.CountCharacter(content);
            htmlBodyBuilder.Append(content);
        }

        public override void visitRoot(DOMRoot rootElement) {
            VisitRootElement(rootElement);
        }

        public override void visitParagraph(DOMParagraph paragraphElement) {
            Statistic.CountParagraph();
            VisitRootElement(paragraphElement);
        }

        public override void visitHeading(DOMHeading inlineElement) {
            VisitRootElement(inlineElement);
        }

        public override void visitSpacingContol(DOMSpacing fillControl) {
            var sizeProp = fillControl.Orientation == DOMSpacing.ORIENTATION_HORIZONTAL ? "width" : "height";
            var styling =
                $"display: inline-block; " +
                $"{sizeProp}: {fillControl.Size}";
            htmlBodyBuilder.Append($"<span style=\"{styling}\"></span>");
        }

        private void VisitRootElement(DOMRoot root) {
            if (root.IgnorableTag()) {
                return;
            }

            if (root.Tag.Equals(HTMLTag.Paragraph)) {
                root.Attributes.SetProperty("id", $"p-{Statistic.ParagraphCount}");
            }

            htmlBodyBuilder.Append($"<{root.Tag}");
            if (root.StyleClasses.Count > 0) {
                htmlBodyBuilder.Append($" class=\"{string.Join(' ', root.StyleClasses)}\"");
            }
            if (root.InlineStyle.Count > 0) {
                htmlBodyBuilder.Append($" style=\"");
                foreach (var kv in root.InlineStyle) {
                    htmlBodyBuilder.Append($"{kv.Key}: {kv.Value}; ");
                }
                htmlBodyBuilder.Append("\"");
            }
            foreach (var kv in root.Attributes) {
                htmlBodyBuilder.Append($" {kv.Key}=\"{kv.Value}\"");
            }

            if (root.Type == DOMType.Void) {
                htmlBodyBuilder.Append("/>");
            }
            else {
                htmlBodyBuilder.Append(">");
                foreach (var el in root.Children()) {
                    el.Accept(this);
                }
                htmlBodyBuilder.Append($"</{root.Tag}>");
            }
        }
    }
}
