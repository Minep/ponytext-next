﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Renderer;
using System.IO;
using System.Text;

namespace PonyTextNext.Renderer.HTML
{
    [Renderer("html")]
    public class HTMLRenderer : RendererBase
    {
        private HTMLVisitor visitor;
        private StringBuilder html;

        public HTMLRenderer(DOMElement DOMRoot, PonyTextContext setting)
            : base(DOMRoot, setting) {
            visitor = new HTMLVisitor();
            html = new StringBuilder();
        }

        public override MemoryStream Render() {
            html.Clear();
            html.Append("<!DOCTYPE html>");
            html.Append("<html><head>");

            populateHtmlHeader();

            html.Append("</head><body>");

            DOM.Accept(visitor);
            html.Append(visitor.Body);

            html.Append("</body></html>");

            Statistic = visitor.Statistic;

            return new MemoryStream(Encoding.UTF8.GetBytes(html.ToString()));
        }

        public string HTML => html.ToString();

        private void populateHtmlHeader() {
            html.Append("<meta charset=\"utf-8\">");
            html.Append($"<title>{setting.DocumentConfig.Title}</title>");
            html.Append("<style>");
            html.Append(setting.StyleSheet.ToCss());
            html.Append("</style>");
            html.Append("<script type=\"text/javascript\">");
            html.Append("var $heading = new Map([");
            foreach (var kv in setting.HeadingTracker) {
                html.Append($"['{kv.Key}', {kv.Value}],");
            }
            html.Append("])</script>");
        }
    }
}
