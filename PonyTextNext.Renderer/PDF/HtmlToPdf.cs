﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Renderer;
using PonyTextNext.Renderer.HTML;
using System;
using System.IO;

namespace PonyTextNext.Renderer.PDF
{
    // [Renderer("pdf-html")]
    public class HtmlToPdf : RendererBase
    {
        private HTMLRenderer renderer;
        public HtmlToPdf(DOMElement DOM, PonyTextContext setting) : base(DOM, setting) {
            renderer = new HTMLRenderer(DOM, setting);
        }

        public override MemoryStream Render() {
            throw new NotImplementedException();
        }
    }
}
