﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.ParagraphLevel;
using System.Text;

namespace PonyTextNext.Renderer.Markdown
{
    public class MarkdownVisitor : DOMVisitorBase
    {
        private StringBuilder mdBuilder;

        public MarkdownVisitor() {
            mdBuilder = new StringBuilder();
        }

        public override void visitHeading(DOMHeading inlineElement) {
            mdBuilder.Append($"{new string('#', inlineElement.Level + 1)} ");
            foreach (var item in inlineElement.Children()) {
                item.Accept(this);
            }
            mdBuilder.Append("\n\n");
        }

        public override void visitLeaf(DOMLeaf leafElement) {
            mdBuilder.Append(leafElement.Content);
        }

        public override void visitParagraph(DOMParagraph inlineElement) {
            foreach (var item in inlineElement.Children()) {
                item.Accept(this);
            }
            mdBuilder.Append("\n\n");
        }

        public override void visitRoot(DOMRoot rootElement) {
            foreach (var item in rootElement.Children()) {
                item.Accept(this);
            }
        }

        public override void visitInline(DOMInline inlineElement) {
            var tagName = inlineElement.Tag;
            switch (tagName) {
                case HTMLTag.Strong:
                    mdBuilder.Append("**");
                    base.visitInline(inlineElement);
                    mdBuilder.Append("**");
                    break;
                case HTMLTag.Italic:
                    mdBuilder.Append("*");
                    base.visitInline(inlineElement);
                    mdBuilder.Append("*");
                    break;
                default:
                    break;
            }
        }
    }
}
