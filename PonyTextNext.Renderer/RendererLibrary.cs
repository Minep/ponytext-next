﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Renderer;
using PonyTextNext.Api.Utilities;
using System;
using System.Collections.Generic;

namespace PonyTextNext.Renderer
{
    public class RendererLibrary : IRendererLibrary
    {
        public IEnumerable<Type> GetAllRenderer() {
            return GetType().GetAsmTypesWithAttribute<RendererAttribute>();
        }
    }
}
