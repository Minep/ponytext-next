# PonyText (v2)
A meta-programming language for document generation.

## What in tarnation is this exactly?

Well, you can treat it like a much simplified and constrained version of LaTeX, or a "pure text" version of Microsoft Word in case if you don't know LaTeX yet. However, the PonyText offers yet simple, concise and straightforward way to describe the layout of document. 

Unlike LaTeX which is designated to become an "omnipotent" typesetting system. The motivation behind PonyText is quite simple, it is designed to solve the issue I have encountered during my fiction-writing, as we shall see.

### Once upon a time...

As CS people often mentioned: "There are two major difficulties in computer science: caching and naming things." Except the caching part, the assertion for naming stuff also a difficulties in everywhere. So during my journey of writing a fiction, naming a characters usually falls within the "abyss of challenges" whilst you can't push the story by any inches forward without these ingredients. To resolve such contradiction, what I usually do is to create a "temporary name" which based solely on character's background setting. That is a very rough and unpolished version of name, or even a dummy placeholder. And after somewhere in the journey, a spark of inspiration flash through my mind, so I decide to backtrace and replace the "temporary name" of a character with the ingenious name of which the creation of the sparkle. 

But that is a very time consuming and easy-to-make-mistake process especially when your story goes like tens of thousand of words away from this name first appeared. And that is where PonyText comes, it introduce the idea of "macro" which kind of like a placeholder but it will be replaced with the actual value automatically by PonyText. So the built in macro preprocessing mechanism which will do this favour for you.

### Using "Replace-All" seems works in Microsoft Words, why don't you go for that?

That true, Microsoft Words is indeed a great software that provide "you see what you get" experience. But the thing is, when you have more than millions of words and numerous chapters and sections inside your work, it could be a troublesome if you wish to navigate to some specific point in your literature and not to mention re-organizing chapters and sections. 

There is another issue I encounter frequently. The MS Words seems unstable and crashed upon save or on fast-pace editing, especially when you get too much characters in your document (for me, my first fiction got about 520,000 Chinese characters in the docx, and crash sometimes)

Also, the MS Words makes the apply of version control difficult and imprecise. Although I know that is weird idea, and seems like the "Frankenstein's creature" resulting from the consequence when programmer try to take his foot in creative-writing, that is actually quite awesome and worth to try out. From some perspective, it offer you a safest backup option for your masterpiece and the way-back machine for your mistakes. For instance, when you progress the plot into wrong direction and you can just simply revert to your nearest checkpoint at no harm.


### So, in that case, why don't you just go for LaTeX?

Yeah, LaTeX is great and marvelous!

However, if you had ever used LaTeX especially try to learn it in first place, you will find out the LaTeX is really really hard and complicated. It is also a nightmare if you want to use LaTeX to render non-English texts (I am not relating to a LaTeX guru). Moreover, in terms of writing a fiction (no maths, no figures), using LaTeX seems like an overkill.

I don't believe a novelist will try to digest the complication of LaTeX. Then writing something like `\usepackage`, `\documentclass`, `\begin...\end` and trying to figure out what is the meaning of square bracket, brace and other fancy reserved characters when he/she just want to head straight to writing. And not to mention about getting weird error on their first attempt and spend probably hours on Googling about the solutions. (Of course, assume they like to write directly on their computer)

The error message in LaTeX is also not very friendly and seems like being obfuscated for beginner.

### And that is how PonyText designed.

In a nutshell, the PonyText ...

+ Made for novelist by novelist.
+ Designed to be simple and easy, with zero setup, just like your little cute pony. All you need to do just start typing.
+ Always follow KISS principle. There are fewer chances of making mistake in PonyText.
+ Able to include other source files.
+ Equipped with sophisticated macro preprocessor, like command system in LaTeX.
+ Preferred output format is HTML. So it also support including CSS and Javascript from the source file. It offers much freedom of styling. In some degree, currently, it is essentially a meta-programming language for HTML. But other output formats is also under development and consideration.
+ Have already been used for backing up my work-in-progress fiction (~180,000 Chinese characters when this README is written)

## Get Started

Just create a plain text file and start typing, each paragraph is separated by one or more hits of 'enter' key. After that you just invoke the PonyText from command line:

```shell
ptext render -i in.txt -o out.html
```

For no good reason, we recommend you to use `.pony` as extension to separate normal plain text file from PonyText file. As always, it's up to you.

That is basic feature it provides. To use all feature, please read on.

## The Big ol'Macros

Let's start digging into the awesome functionalities in PonyText!

Now, let's say you are kind of person who have huge difficulties on naming things. You comes up with a characters, and a fancy place. And decide to assign the following placeholder names for no good reason, `JohnSmith` and `Crazy-Place`. 

To make use of macro system in PonyText, you will writes:


> His name was `@JohnSmith;` and he was just a country boy who had never seen `@Crazy-Place;` before.

Pay attention to the highlighted part, that is how you reference a macro. Such reference always follows the format: `@` + name of macro you interested + `;` As followed from the above example, we can introduce the naming convention in PonyText:  **A valid name must comprise all 26 English letters in either upper or lower case, digits of 0-9, an underscore `_`, a dollar `$` and a hyphen `-`. But must not starting with digits and hyphen. While dollar sign can be only used as leading character.** 

So these are the invalid: `-invalid`, `03inva$lid`. While these are the valid: `valid-name-1`, `validName1`, `$valid_name_1`.

However, if you run PonyText against it, you will get a error message complaining `JohnSmith` and `CrazyPlace` are not defined. That's perfectly normal, so you just need to define these marcos with some initial values by appending the definition statement before any reference to the macros. Luckily, you got inspiration, feeling like the mercy Father of All Sci-Fi, the Almighty God of Imagination - Asimov the Oracle, just blessed upon you. You decide the name `Gaal Dornick` for this character and `Trantor` for the crazy place you ever imagine:

> ``@JohnSmith <- `Gaal Dornick` ;``
> 
> ``@Crazy-Place <- `Trantor` ;``
> 
> His name was `@JohnSmith;` and he was just a country boy who had never seen `@Crazy-Place;` before.

After running the PonyText, you will get:

> His name was Gaal Dornick and he was just a country boy who had never seen Trantor before.

You will see how much straightforward the declaration is! It will always follows the format: `@` + name of marco + `<-` + value + `;`. But there is one caveat: the declaration statement must be in a dedicate line. This is a by-design behavior. After all, you need clean code style in programming and not to mention in doing literature.

You will also encounter the first data type in PonyText - `string`. Quite different from other programming languages, the string is wrapped by the backticks not double quotes or single quotes. You can also include a backticks in your string, just use `\` to escape it, for example, to define string ``a`b`` you should write `` `a\`b` ``

Apart from string, PonyText also support `number` data type. well, it just number, like `0.1`, `123`, but not `2e3`. The followings are the valid number:

+ `123` or `-123`
+ `0.3` or `-0.3`
+ `.3` or `-.4`

Sadly, PonyText does not support any kind of arithmetic natively. Who need to do maths when writing the fiction by all means? However, the number data type will have special purpose as we will see later.

All macro values, regardless of its data type, will be transformed into their text equivalence when rendering the final document.

And that is all the basic about how you reference a macro. In the next part, we will see some advanced usage on macro referencing.

### Macro can be smart.

Sometimes, you may wish to emphasis some words in your creation to bring up reader's extra attention. In order to do that, PonyText provide two built in macro named `i` (for *italic*) and `b` (for **bold**). But how do you tell this macro the text to emphasis? That will give you the introduction of PonyText philosophy: "Everything is function". So a macro is a function. Let's say, you wish to emphasis the "never" in the sentence using italic and text whatever represent by macro `Crazy-Place` using bold:

> ``@JohnSmith <- `Gaal Dornick` ;``
> 
> ``@Crazy-Place <- `Trantor` ;``
> 
> His name was `@JohnSmith;` and he was just a country boy who had ``@i(`never`);`` seen `@b(Crazy-Place);` before.

The outcome will be:

> His name was Gaal Dornick and he was just a country boy who had *never* seen **Trantor** before.

You will now see, like all other programming language, the parameter passed into macro (or function, but it could be better to understand in the way of "parameterized macro" for the upcoming topics) is enclosed by two parentheses and separated by comma if more than one parameters are being passed.

Pay attention that the way we pass `Crazy-Place`, we discard the `@` and `;` because we do not need tell PonyText where is the **macro context** (this refer to the part where PonyText need to applied marco substitution).

Let's suppose somehow you need to make the words starting from `and he` to the end of sentence to be bold. How can we pass a thing which has both text and macro reference got mixed up? You can do the following:

> ``@JohnSmith <- `Gaal Dornick` ;``
> 
> ``@Crazy-Place <- `Trantor` ;``
> 
> His name was `@JohnSmith;` and `@b(%` he was just a country boy who had never seen `@Crazy-Place;` before. `%);`

The outcome will be:

> His name was Gaal Dornick and **he was just a country boy who had never seen Trantor before.**

The pair of `%` enclose the so called **text context**. The entire source file can be regarded as a big single text context, it is a general representation of a fragment of source file. In this case, it is equivalent to pass a another source file as argument to another macro. Thus, it lead to the thrid data type in PonyText: `Document`. As people would guess and they are right: the `Document` data type can be nested.

How can a starting of the masterpiece without a proper heading? So you decide to add heading of this very first chapter. You can make use of another built in macro and do the following:

> ``@JohnSmith <- `Gaal Dornick` ;``
> 
> ``@Crazy-Place <- `Trantor` ;``
> 
> ``@heading(1, text: `The Psychohistorians`);``;
> 
> ``@heading(2);``;
> 
> His name was `@JohnSmith;` and `@b(%` he was just a country boy who had never seen `@Crazy-Place;` before. `%);`

What are these all means? To understand them, let see the parameter accepted by the `heading` macro: the first parameter is the level of heading, it values ranges from 1 to 6, where 1 is highest level, 6 is the lowest level. Like the relation of chapter, section, subsection, subsubsection and etc.; The second parameter, however optional, it is the text to be written in the heading. Why is that optional? Well, for the sections in a chapter, you don't wish to give a dedicate subtitle to each of them. (as I said, naming things are really really really difficult!) But how in the wide-world of Earth do PonyText know which goes into which if there are many optionals? In order to resolve this ambiguity, PonyText require all optional parameters to have their unique identifier and must specify the name of the optional you wish to pass a value. So that is what the prefix `text : ` do, it tells the PonyText "string `The Psychohistorians` belongs to optional parameter named `text`". And here is the general format: name of optional + `:` + value to this optional. And now it should be trivial for the meaning of `@heading(2);` right beneath it.

The above code will give you the output for **default setting** (the settings will be described later):

> 1.The Psychohistorians
> 
> 1.1
> 
> His name was Gaal Dornick and **he was just a country boy who had never seen Trantor before.**

### Make Yourself a Macro

So the macro system is essentially the core of PonyText functionalities. Any operations and effects can be done by applying macros (sounds a bit like the functional programming, and it is actually one of the influences to the PonyText philosophy). If macros play such important roles in PonyText, then it would be a crime if you can't create your own one.

Let's say you want to create a shortcut for ``@heading(1, text: `The Psychohistorians`);``. And name it to `chapter` so you can define your chapter heading in a much more convenience and straightforward way: ``@chapter(`The Psychohistorians`);``. How to do that? You can just use the arrow assignment as we see at the very beginning:

```
@chapter <- heading(1, text: $0);
```

Now macro `chapter` will have the meaning of `heading`, every time you reference `chapter` and it is equivalent to reference `heading`. Pay attention to `$0`, that is a special built in variable which only available in this assignment context. It refer to the first argument we passed into `chapter`. In this case, when we call ``@chapter(`The Psychohistorians`);`` and the `$0` will have the value of `` `The Psychohistorians` ``.

Similarly, the second argument will go to `$1`, `$2` for third, and so on. There is no limit on the number of argument passed to macro. And in fact, you can pass any number of argument into the self-defined macro even if they are not used. For example, the following invocation is valid:

```
@chapter(`The Psychohistorians`, 1, 2, `c`);
```

Now the `$1`, `$2` and `$3` will be available automatically and have the value of `1`, `2`, `` `c` `` respectively. Even if you are only using `$0`.

Let's move on and make a short cut for `@heading(2);`:

```
@section <- heading(2);
```

So you can invoke `@section();` directly. Note that if there is no need to pass any argument, instead of writing empty parenthesis, you can omit them and write `@section;` - just like the plain ol'macro we saw at the first place.

For multiple marcos, you can just assign a `Document` comprised of macros. For example:

```
@function <- %
    @f1($0);
    @f2($1);
    This is a @i(`function`);
%;
```

If you call the `function` like:

> ``@JohnSmith <- `Gaal Dornick` ;``
> 
> ``@Crazy-Place <- `Trantor` ;``
> 
> ``@function(1, `a`);``
> His name was `@JohnSmith;` and `@b(%` he was just a country boy who had never seen `@Crazy-Place;` before. `%);`

Then, after PonyText macro expansion, it is equivalent to

> ``@JohnSmith <- `Gaal Dornick` ;``
> 
> ``@Crazy-Place <- `Trantor` ;``
> 
> `@f1(1);`
> 
> ``@f2(`a`);``
> 
> This is a ``@i(`function`);``
> 
> His name was `@JohnSmith;` and `@b(%` he was just a country boy who had never seen `@Crazy-Place;` before. `%);`

In conclusion, it is just no more than a parameterized text context fragment.

#### A Known Limitation (Resolved)

**This issue is resolved initially by c967eb73 and improved with subtree-expansion techniques by 7b7c2d74** 

**The content here is for reference only**

To illustrate this limitation, let's consider the following snippet:

```
@foo <- @i($0);
@bar <- foo(heading(1, text: $0));

@bar(`ABC`);
```

What was the result? well, that's a silly question because by our common-sense, it should be a italic heading with text "ABC". But in fact, PonyText will just being stuck in a infinite loop and finally reach the stack overflow state.

Why is that? To understand this, we first see how a macro is evaluated. Take the above snippet as example, when `bar` is invoked, it's arguments will be warped into a structure called `call frame`, and it will then pushed to a stack, namely `call stack`. The top of stack will be the current call context, where the PonyText should look up for the value of `$0`, `$1` and so on. 

After `bar` is considered, the top of stack will comprise ``$0=`ABC` ``. PonyText will then locate `foo` as it is definition of `bar`. `foo` is evaluated, and top of stack will be ``$0=heading ``. Finally, the built-in macro `i` will be evaluated and PonyText will invoke the C# code behind the `i` to compute the output. During the execution of code, all the argument will be handed back to PonyText again to evaluate (if your argument is another macro, you certainly want the actual value behind it to be processed but not macro itself). 

And here is the problem arose. When `heading` is evaluated, and the `$0` argument is also evaluated. Remember the current context is ``$0=heading ``, so `heading` will be evaluated again! That will cause the infinite loop happened.

In a nutshell, this limitation is because PonyText evaluate things by going down the `document tree` (we will get much detail on this when explaining how PonyText works) without the awareness of the parameter context.

This problem will be fixed in the future. For now, we have suggest a possible workaround to this issue, by passing through the argument directly into that nested call context:

```
@foo <- @i($0);
@bar <- foo(heading(1, text: $1), $0);

@bar(`ABC`);
```

So when `foo` is evaluated, the call context will be `foo.$0=heading`, ``foo.$1=bar.$0=`ABC` ``. When `heading` is being evaluated in `foo` context, you will get expected argument to be passed.

### Settings Matters

PonyText is designed to be zero configuration, but it doesn't mean you can't do any customization. Each built in marco will have configurations for their behavior. So this part is all about those built in macro. And we can see the differences between built in macro and user defined macro have been diverged, thus it is not suitable to call them "build macro". So we will give those built-ins a different name: Transformer. Because they are transforming source into final result.

For example, the transformer `heading` allow you to decide whether to enabling auto-numbering, where the numbering starts from, and the heading level-wise control of format of numbering.

Suppose you want to add prefix `Chapter x` before your chapter title and a single number for section numbering, all numbering starts from one. You might write:

```
@config(`transformer.heading`, !prop`{
    "enableNumbering": "true",
    "startsFrom": 1,
    "fmt@0": "Chapter {0:en}: ",
    "fmt@1": "{1}"
}`);
```

Seems like a bit overwhelming. Let's look one by one! First, we introduce a new transformer: `config`. Which it takes two parameter: the category of configuration and the configuration itself. To set a transformer's config, you need to formulate your category using the format: `transformer.{name}`. In the above example, we are setting the configuration for `heading`.

The second argument, well, it is another data type in PonyText: `Structured String`. It has the following format: `!` + structure name (same naming convention as macro) + string follows that structure. In the above case, the string will follow a `prop` structure, and the `prop` structure is essentially any valid JSON.

Let's look on the configuration terms, the first two are self-explanatory. The `fmt@0` is simply the format of level 1, it is count from 0, so `fmt@i` will be the format of level i+1. And this same as the `{0}` and `{1}` in the string, it will be replaced by the counts in equivalent level (based 0). 

Note that, in `{0:en}` the `en` means verbalize the numbering using English, so if your numbering is `12`, it will give you `twelves`. More information will be available in the Transformer part.

After that setting is done, the source:

> ``@JohnSmith <- `Gaal Dornick` ;``
> 
> ``@Crazy-Place <- `Trantor` ;``
> 
> ``@heading(1, text: `The Psychohistorians`);``
> 
> ``@heading(2);``
> 
> His name was `@JohnSmith;` and `@b(%` he was just a country boy who had never seen `@Crazy-Place;` before. `%);`

will become:

> Chapter One: The Psychohistorians;
> 
> 1
> 
> His name was `@JohnSmith;` and `@b(%` he was just a country boy who had never seen `@Crazy-Place;` before. `%);`

However, using `config` will apply the configuration to all subsequent use of that particular transformer. If you want to change the configuration for only one usage. You can use a built-in optional parameter, `$option`, which is available to all transformer.

### Including the other source file

How can I forget this! You can certainly split up your million words masterpiece into different files, for example, one for each chapter. So you will never get lost in the "ocean of letters" when navigating through the document.

Suppose you have two file `chapter1.pony` and `index.pony`, all located under same director:

```
masterpiece
 ├ chapter1.pony
 └ index.pony
```

then you can simply include the `chapter1.pony` really quick by writing:

```
@@ `chapter1.pony`;
```

on somewhere in your `index.pony`. When the rendering is complete, the content from `chapter1.pony` will get replaced `` @@ `chapter1.pony`; `` at the exact same location.

If you got subdirectory:

```
masterpiece
 ├ chapters
 │   └ chapter1.pony
 └ index.pony
```

that's fine, you just write the relative path to the file you need to include.

```
@@ `chapters/chapter1.pony`;
```

The path of the included file will always relative to current directory where the inclusion happened. And the dot notation is also supported. (for example, `../` and `./`)

There is also a shorthand supported by PonyText, for instance, if you write:

```
@@ `chapters`;
```

this could have two meaning: include a file named `chapters` or include a directory named `chapters`. PonyText will first check the file, if not found however, go for directory. If the latter is the case, then it is implied to look for file `chapters/index.pony`

#### Cyclic Dependency

PonyText hates cyclic dependency. So, for example, if you do something like that:

```
--- a.pony ---
@@ `b.pony`;
```

```
--- b.pony ---
@@ `a.pony`;
```

Then PonyText will refuse to compile and will show you the problematic dependency path so you can go fix the issue quickly.

## That's All

That's all you need to know about how PonyText is motivated and how you to utilizing PonyText for enhance your creative writing for the most of the case. 

However, these are not the boundary. PonyText comes with lot's of transformer designed for different tasks and also many other features!

To get yourself absorbed into the very detailed, advanced usage and magic behind the scene. Just head straight to the API documentation (work in progress) !