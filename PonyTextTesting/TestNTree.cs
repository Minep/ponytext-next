﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using NUnit.Framework;
using PonyTextNext.Api.Utilities;
using System.Text;

namespace PonyTextTesting
{
    public class TestNTree
    {
        class ExampleTreeNode : NTreeRoot<ExampleTreeNode>
        {
            public string content { get; }
            public ExampleTreeNode(string content) {
                this.content = content;
            }
            public ExampleTreeNode() {
                this.content = "<root>";
            }
        }

        ExampleTreeNode root;

        /*
         * 
         * r
         * |
         * 0 1 r 10 11   r
         *     |         |
         *     2 3 4 r   7 8 9
         *           |
         *           5 6
         */
        [SetUp]
        public void setup() {
            root = new ExampleTreeNode();
            var root1 = new ExampleTreeNode();
            var root2 = new ExampleTreeNode();
            var root3 = new ExampleTreeNode();
            var counter = 0;
            for (int i = 0; i < 2; i++) {
                root.AddToChildren(new ExampleTreeNode(counter.ToString()));
                counter++;
            }
            root.AddToChildren(root1);

            for (int i = 0; i < 3; i++) {
                root1.AddToChildren(new ExampleTreeNode(counter.ToString()));
                counter++;
            }
            root1.AddToChildren(root2);

            for (int i = 0; i < 2; i++) {
                root2.AddToChildren(new ExampleTreeNode(counter.ToString()));
                counter++;
            }

            for (int i = 0; i < 3; i++) {
                root3.AddToChildren(new ExampleTreeNode(counter.ToString()));
                counter++;
            }

            for (int i = 0; i < 2; i++) {
                root.AddToChildren(new ExampleTreeNode(counter.ToString()));
                counter++;
            }
            root.AddToChildren(root3);
        }

        [Test]
        public void TestFlatternToList() {
            var node = root.FlattenToList();
            var sb = new StringBuilder();
            foreach (var item in node) {
                sb.Append(item.content);
            }
            Assert.AreEqual("01234561011789", sb.ToString());
        }
    }
}
