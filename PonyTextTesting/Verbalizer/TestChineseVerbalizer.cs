﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using NUnit.Framework;
using PonyTextNext.Transformers.Utils.Verbalization;

namespace PonyTextTesting.Verbalizer
{
    public class TestChineseVerbalizer : ChineseVerbalizer
    {
        public TestChineseVerbalizer() {

        }
        [Test]
        public void TestGetUnit1Digit() {
            Assert.AreEqual("", digitPlaceToUnit(1));
        }

        [Test]
        public void TestGetUnit2Digit() {
            Assert.AreEqual("十", digitPlaceToUnit(2));
        }

        [Test]
        public void TestGetUnit5Digit() {
            Assert.AreEqual("万", digitPlaceToUnit(5));
        }

        [Test]
        public void TestGetUnit7Digit() {
            Assert.AreEqual("百万", digitPlaceToUnit(7));
        }

        [Test]
        public void TestGetUnit10Digit() {
            Assert.AreEqual("十亿", digitPlaceToUnit(10));
        }

        [Test]
        public void TestGetUnit13Digit() {
            Assert.AreEqual("万亿", digitPlaceToUnit(13));
        }

        [Test]
        public void TestGetUnit14Digit() {
            Assert.AreEqual("十万亿", digitPlaceToUnit(14));
        }

        static object[] VerbalizationCases =
        {
            new object[]{80, "八十"},
            new object[]{0, "零"},
            new object[]{13, "一十三"},
            new object[]{1023000, "一百零二万三千"},
            new object[]{108, "一百零八"},
            new object[]{1008, "一千零八"},
            new object[]{54, "五十四"},
            new object[]{823432, "八十二万三千四百三十二"},
            new object[]{82343200, "八千二百三十四万三千二百"},
            new object[]{800000, "八十万"},
            new object[]{-800001, "负八十万零一"},
            new object[]{100000003, "一亿零三"},
            new object[]{100002003, "一亿零二千零三"},
            new object[]{1100002003, "一十一亿零二千零三"},
        };

        [TestCaseSource(nameof(VerbalizationCases))]
        public void TestVerbalize(int test, string expected) {
            Assert.AreEqual(expected, verbalizeInteger(test.ToString()));
        }

    }
}
