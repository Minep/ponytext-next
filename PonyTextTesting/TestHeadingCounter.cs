﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using NUnit.Framework;
using PonyTextNext.Api.Utilities;

namespace PonyTextTesting
{
    public class TestHeadingCounter
    {
        [Test]
        public void TestCountIndividual() {
            var hc = new HeadingCounter();
            hc.Count(1);
            hc.Count(1);
            hc.Count(1);
            hc.Count(1);
            Assert.AreEqual("1.4.1.1.1.1", string.Join('.', hc.Counters));
        }

        [Test]
        public void TestCountIndividualLowerLevel() {
            var hc = new HeadingCounter();
            hc.Count(1);
            hc.Count(1);
            hc.Count(1);
            hc.Count(2);
            hc.Count(2);
            Assert.AreEqual("1.3.2.1.1.1", string.Join('.', hc.Counters));
        }

        [Test]
        public void TestCountUpperLevel() {
            var hc = new HeadingCounter();
            hc.Count(2);
            hc.Count(2);
            hc.Count(5);
            hc.Count(1);
            hc.Count(1);
            hc.Count(1);
            Assert.AreEqual("1.3.1.1.1.1", string.Join('.', hc.Counters));
        }

        [Test]
        public void TestCountUpperLevelBase0() {
            var hc = new HeadingCounter();
            hc.InitialValue = 0;
            hc.Count(2);
            hc.Count(2);
            hc.Count(5);
            hc.Count(1);
            hc.Count(1);
            hc.Count(1);
            Assert.AreEqual("0.2.0.0.0.0", string.Join('.', hc.Counters));
        }
    }
}
