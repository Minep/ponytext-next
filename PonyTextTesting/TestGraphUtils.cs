// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using NUnit.Framework;
using PonyTextNext.Api.Utilities;
using System.Collections.Generic;

namespace PonyTextTesting
{
    public class Tests
    {
        [SetUp]
        public void Setup() {
        }

        /*
         *   1 <--\
         *   ^    |
         *   \____/
         *        
         */
        [Test]
        public void TestSelfLoop() {
            DirectedGraph<int> graph = new DirectedGraph<int>();
            graph.PutArrow(1, 1);
            var path = graph.GetCyclicPathFrom(1);
            var expected = new List<int>() { 1, 1 };
            CollectionAssert.AreEqual(expected, path);
        }

        /*
         * 1 -> 2 -> 3
         *      ^    |
         *      \____/
         */
        [Test]
        public void TestLoop2() {
            DirectedGraph<int> graph = new DirectedGraph<int>();
            graph.PutArrow(1, 2);
            graph.PutArrow(2, 3);
            graph.PutArrow(3, 2);
            var path = graph.GetCyclicPathFrom(1);
            var expected = new List<int>() { 1, 2, 3, 2 };
            CollectionAssert.AreEqual(expected, path);
        }

        /*
         * 1 -> 2 -> 3 -> 4 -> 5
         *      \_________^
         */
        [Test]
        public void TestNonLoop() {
            DirectedGraph<int> graph = new DirectedGraph<int>();
            graph.PutArrow(1, 2);
            graph.PutArrow(2, 3);
            graph.PutArrow(3, 4);
            graph.PutArrow(4, 5);
            graph.PutArrow(2, 4);
            var path = graph.GetCyclicPathFrom(1);
            CollectionAssert.IsEmpty(path);
        }

        /*
         *            _________
         *           /         \
         *           v         |
         * 1 -> 2 -> 3 -> 4 -> 5
         *           |
         *           v
         *      8 <- 6 -> 7
         */
        [Test]
        public void TestLoop3() {
            DirectedGraph<int> graph = new DirectedGraph<int>();
            graph.PutArrow(1, 2);
            graph.PutArrow(2, 3);
            graph.PutArrow(3, 4);
            graph.PutArrow(3, 6);
            graph.PutArrow(6, 7);
            graph.PutArrow(6, 8);
            graph.PutArrow(4, 5);
            graph.PutArrow(5, 3);
            var path = graph.GetCyclicPathFrom(1);
            var path2 = graph.GetCyclicPathFrom(3);

            var expected = new List<int>() { 1, 2, 3, 4, 5, 3 };
            var expected2 = new List<int>() { 3, 4, 5, 3 };
            CollectionAssert.AreEqual(path, expected);
            CollectionAssert.AreEqual(path2, expected2);
        }
    }
}