﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using NUnit.Framework;
using PonyTextNext.Transformers.Utils.Verbalization;

namespace PonyTextTesting
{
    public class TestVerbalizationFormatter
    {
        [Test]
        public void TestFormatAlphaNumeric() {
            var fmt = "{0:num} - {1:num}";
            var fmtProvider = new VerbalizationFormatter();
            Assert.AreEqual("1 - 4", string.Format(fmtProvider, fmt, 1, 4));
        }

        [Test]
        public void TestFormatChineseLocale() {
            var fmt = "{0:ch} - {1:ch}";
            var fmtProvider = new VerbalizationFormatter();
            Assert.AreEqual("二 - 八十", string.Format(fmtProvider, fmt, 2, 80));
        }

        [Test]
        public void TestFormatChineseLocaleCharwised() {
            var fmt = "{0:CH} - {1:CH}";
            var fmtProvider = new VerbalizationFormatter();
            Assert.AreEqual("二三 - 八零", string.Format(fmtProvider, fmt, 23, 80));
        }
    }
}
