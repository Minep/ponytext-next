﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using CommandLine;
using PonyTextNext.Options;
using Serilog;

namespace PonyTextNext
{
    class Program
    {
        static void Main(string[] args) {
            var core = new AppCore();
            Parser.Default
                .ParseArguments<RenderOptions, TransformerOptions, InspectOptions>(args)
                .MapResult(
                    (RenderOptions opts) => core.DispatchAction("render", opts),
                    (TransformerOptions opts) => core.DispatchAction("transformer", opts),
                    (InspectOptions opts) => core.DispatchAction("inspect", opts),
                    err => 3
                );
            Log.CloseAndFlush();
        }
    }
}
