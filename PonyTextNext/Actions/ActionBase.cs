﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api;
using PonyTextNext.Core;
using PonyTextNext.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Actions
{
    public abstract class ActionBase<T> where T : OptionBase
    {
        protected PonyText PonyText;
        protected PonyTextContext Context;

        protected ActionBase(PonyText ponyText, PonyTextContext context) {
            PonyText = ponyText ?? throw new ArgumentNullException(nameof(ponyText));
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public abstract void Dispatch(T options);
    }
}
