﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api;
using PonyTextNext.Core;
using PonyTextNext.Options;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Actions
{
    public class ActionTransformer : ActionBase<TransformerOptions>
    {

        public ActionTransformer(PonyText ponyText, PonyTextContext context): base(ponyText, context) {

        }

        public override void Dispatch(TransformerOptions options) {
            if (options.ListTransformer) {
                Log.Information("These are the transformer supported:");
                foreach (var item in PonyText.TransformerRegistry.GetRegisteredTransformers()) {
                    Log.Information("  * {0}", item);
                }
                return;
            }
        }
    }
}
