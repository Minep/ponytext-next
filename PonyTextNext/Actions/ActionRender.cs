﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Core;
using PonyTextNext.Options;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PonyTextNext.Actions
{
    public class ActionRender : ActionBase<RenderOptions>
    {
        public ActionRender(PonyText ptext, PonyTextContext context) : base(ptext, context) {

        }

        public override void Dispatch(RenderOptions options) {
            PonyText.InjectMacros(options.MacroDefinitions);
            PonyText.Load(options.Input);
            PonyText.RenderToFile(options.Using, options.Output);

            if (!string.IsNullOrEmpty(options.ExportDependencyGraph)) {
                var path = Path.Combine(Context.BasePath, options.ExportDependencyGraph);
                File.WriteAllText(path, PonyText.Resolver.GetDependencyGraph());
                Log.Information($"Exported graph to: {options.ExportDependencyGraph}");
            }

            Log.Information($"Exported content to: {options.Output}");
        }
    }
}
