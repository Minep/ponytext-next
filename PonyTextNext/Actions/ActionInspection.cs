﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api;
using PonyTextNext.Api.Document;
using PonyTextNext.Api.Document.Fragments;
using PonyTextNext.Core;
using PonyTextNext.Options;
using Serilog;

namespace PonyTextNext.Actions
{
    public class ActionInspection : ActionBase<InspectOptions>
    {
        public ActionInspection(PonyText ponyText, PonyTextContext context) : base(ponyText, context) {
        }

        public override void Dispatch(InspectOptions options) {
            PonyText.Load(options.Input);
            Log.Information("");
            if (options.Macros) {
                foreach (var item in PonyText.Engine.SymbolTable) {
                    var value = item.Value;
                    string definition = "<non-trivial>";
                    if (value is DocumentTerminal) {
                        definition = ((DocumentTerminal) value).AsString();
                    }
                    Log.Information($"  * {{0}} = {definition}", item.Key);
                }
                Log.Information("Found {0} macros", PonyText.Engine.SymbolTable.Count);
            }
            else {
                var macro = PonyText.Engine.SymbolTable.ReadRegisterSafe(options.MacroToRead);
                if (macro is DocumentTerminal) {
                    Log.Information(" {0} = `{1}`", options.MacroToRead, ((DocumentTerminal) macro).AsString());
                }
                else {
                    Log.Warning(" Macro '{0}' is non-trivial.", options.MacroToRead);
                    Log.Warning(" However, we will show it to you, in PonyText Markup Language");
                    Log.Warning("");
                    Log.Information("{0}", macro.GetText());
                }
            }
        }
    }
}
