﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api;
using PonyTextNext.Api.Document;
using PonyTextNext.Api.Inject;
using PonyTextNext.Api.Renderer;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Registry;
using PonyTextNext.Core;
using PonyTextNext.Core.ptext.dependency;
using PonyTextNext.Core.Renderer;
using PonyTextNext.Core.Runtime;
using PonyTextNext.Core.Runtime.Invocation;
using PonyTextNext.Core.Runtime.Registries;
using SimpleInjector;
using System;

namespace PonyTextNext
{
    public class DependencyLoader
    {
        private readonly PonyTextContext ponyTextSetting;
        private readonly Container container;

        public DependencyLoader(PonyTextContext ponyTextSetting) {
            this.ponyTextSetting = ponyTextSetting;
            container = new Container();
            container.Options.EnableAutoVerification = false;
            container.RegisterInstance(ponyTextSetting);
            LoadAll();
        }

        private void LoadAll() {
            container.RegisterSingleton<IPonyTextDependencyResolver, PonyTextResolver>();
            container.RegisterSingleton<PonyTextResolver>();
            container.RegisterSingleton<DependencyGraph>();
            container.RegisterSingleton<RegistryBase<DocumentFragmentBase>, SymbolRegistry>();
            container.RegisterSingleton<ITransformerRegistry, TransformerRegistry>();
            container.RegisterSingleton<IMixinInvocator, MixinInvocator>();
            container.RegisterSingleton<ISLiteralEvaluator, JsonSLiteralEvaluator>();
            container.RegisterSingleton<IRendererDispatcher, RendererDispatcher>();
            container.Register<IEvaluationEngine, EvaluationEngine>();
            container.Register<PonyText>();
            // container.Verify();
        }

        public T Bootstrap<T>() {
            return (T) container.GetInstance(typeof(T));
        }

        private void register(Type type, ManagedDependencyAttribute dependencyAttribute) {
            if (dependencyAttribute.Singleton) {
                container.RegisterSingleton(type, type);
            }
            else {
                container.Register(type);
            }
        }
    }
}
