﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using CommandLine;
using PonyTextNext.Options.Templates;
using System.Collections.Generic;

namespace PonyTextNext.Options
{
    [Verb("render", isDefault: true, HelpText = "Render PonyText to various format")]
    public class RenderOptions : OptionBase, IInput
    {
        [Option(HelpText = "Renderer to use for rendering", Default = "html", Required = false)]
        public string Using { get; set; }

        [Option('o', Required = true, HelpText = "PonyText renderer output file")]
        public string Output { get; set; }

        [Option("macros", Required = false, HelpText = "Predefine macros via command line. Sequence of Name=Value")]
        public IEnumerable<string> MacroDefinitions { get; set; }

        [Option("deps-graph",
            Default = null,
            HelpText = "Location for saving the dependency graph. If leave unspecified, PonyText will not generate the graph",
            Required = false)]
        public string ExportDependencyGraph { get; set; }

        public string Input { get; set; }
    }
}
