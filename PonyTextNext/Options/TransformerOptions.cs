﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Options
{
    [Verb("transformer", HelpText = "Operations related to transformer")]
    public class TransformerOptions : OptionBase
    {
        [Option(
            'l', "list", 
            HelpText = "List all transformer supported by PonyText",
            Required = false
        )]
        public bool ListTransformer { get; set; }
    }
}
