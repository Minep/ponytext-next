﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Options.Templates
{
    public interface IInput
    {
        [Option('i', "input", Required = true, HelpText = "PonyText source file")]
        public string Input { get; set; }
    }
}
