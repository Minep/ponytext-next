﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using CommandLine;
using PonyTextNext.Options.Templates;

namespace PonyTextNext.Options
{
    [Verb("inspect")]
    public class InspectOptions : OptionBase, IInput
    {
        public string Input { get; set; }

        [Option("list-macros", SetName = "inspect", HelpText = "list all macros defined")]
        public bool Macros { get; set; }

        [Option("read-macro", SetName = "inspect", HelpText = "Read the value of specific macro")]
        public string MacroToRead { get; set; }
    }
}
