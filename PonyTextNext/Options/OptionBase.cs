﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using CommandLine;

namespace PonyTextNext.Options
{
    public class OptionBase
    {
        [Option("base", Hidden = true, Required = false, Default = null)]
        public string BasePath { get; set; } = null;

        [Option("allow-html", HelpText = "Allow PonyText to accept insecure code, but cation of XSS attack", Required = false, Default = null)]
        public bool AllowHTML { get; set; } = false;
    }
}
