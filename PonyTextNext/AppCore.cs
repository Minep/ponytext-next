﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Actions;
using PonyTextNext.Api;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Core;
using PonyTextNext.Options;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace PonyTextNext
{
    public class AppCore
    {
        private readonly DependencyLoader _loader;
        private readonly PonyTextContext _context;
        private readonly Dictionary<string, Type> _actions;

        public AppCore() {
            _context = new PonyTextContext();
            _actions = new Dictionary<string, Type>();
            _context.BasePath = Environment.CurrentDirectory;
            _loader = new DependencyLoader(_context);

            ConfigLogger();
            RegisterActions();
        }

        private void RegisterActions () {
            _actions.Add("render", typeof(ActionRender));
            _actions.Add("transformer", typeof(ActionTransformer));
            _actions.Add("inspect", typeof(ActionInspection));
        }

        public int DispatchAction(string verb, OptionBase options) {
            Type type;
            if (!_actions.TryGetValue(verb, out type)) {
                Log.Error("Unknown verb '{0}'", verb);
                return 1;
            }
            var ponyText = _loader.Bootstrap<PonyText>();
            var action = Activator.CreateInstance(type, ponyText, _context);
            return RunWithDiagnosis(() => {
                _context.BasePath = options.BasePath ?? Environment.CurrentDirectory;
                _context.AllowHTML = options.AllowHTML;
                type.GetMethod("Dispatch")?.Invoke(action, new object[] { options });
            });
        }

        private void ConfigLogger() {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console(
                    outputTemplate:
                        "[{Timestamp:HH:mm:ss} {Level:u4}] {Message:lj}{NewLine}{Exception}",
                    theme:
                        AnsiConsoleTheme.Literate
                )
                .CreateLogger();
        }

        private int RunWithDiagnosis(Action task) {
            var stopWatch = new Stopwatch();
            var returnCode = 0;

            stopWatch.Start();

            try {
                task.Invoke();
            }
            catch (Exception e) {
                Log.Logger.Error("PonyText has been stopped for an error.");
                PrintPTextExceptionStack(e);
                returnCode = 2;
            }

            stopWatch.Stop();

            using (var process = Process.GetCurrentProcess()) {
                var peakMemMb = process.PeakWorkingSet64 / 1024 / 1024;
                var latest = process.WorkingSet64 / 1024 / 1024;
                Log.Logger.Information("---- PonyText Report ----");
                Log.Logger.Information("Latest memory usage: {0}MiB", latest);
                Log.Logger.Information("Max memory usage: {0}MiB", peakMemMb);
                Log.Logger.Information("Time elapsed: {0:0.##} seconds", stopWatch.ElapsedMilliseconds / 1000d);
            }

            return returnCode;
        }

        private void PrintPTextExceptionStack(Exception e) {
            var list = new Stack<string>();
            Exception exception = e;
            while (exception != null) {
                list.Push(exception.Message);
                exception = exception.InnerException;
            }
            Log.Logger.Error("");
            Log.Logger.Error("+---------------------------------+");
            Log.Logger.Error("|      PonyText Stack Trace       |");
            Log.Logger.Error("+---------------------------------+");
            while (list.Count > 0) {
                Log.Logger.Error(list.Pop());
            }
            Log.Logger.Error("+---------------------------------+");
            Log.Logger.Error("|         End Stack Trace         |");
            Log.Logger.Error("+---------------------------------+");
        }
    }
}
