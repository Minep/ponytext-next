﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;

namespace PonyTextNext.Api.Inject
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public sealed class ManagedDependencyAttribute : Attribute
    {
        public bool Singleton { get; set; }
    }
}
