﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.Runtime.Config
{
    public class DocumentAttribute
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string PageSize { get; set; }
        public double PageWidth { get; set; }
        public double PageHeight { get; set; }
        public bool EnablePageNumbering { get; set; }
    }
}
