﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Runtime.Registry;
using System.Text;

namespace PonyTextNext.Api.Runtime.Config
{
    public class AtRules : ICssCompatible
    {
        private string ruleName;
        private KeyValueProperties ruleStyle;
        private RegistryBase<KeyValueProperties> subRules;

        public AtRules(string ruleName) {
            this.ruleName = ruleName;
            ruleStyle = new KeyValueProperties();
            subRules = new GeneralRegistry<KeyValueProperties>();
        }

        public void SetRuleStyle(KeyValueProperties styles) {
            ruleStyle.OverrideWith(styles);
        }

        public void SetSubRule(string subRule, KeyValueProperties styles) {
            subRules.ComputeValue(subRule, v => {
                if (v == null) {
                    return styles;
                }
                return v.OverrideWith(styles);
            });
        }

        public string ToCss() {
            var sb = new StringBuilder();
            sb.Append($"@{ruleName} {{");
            sb.Append(ruleStyle.ToCss());
            foreach (var kv in subRules) {
                sb.Append($"{kv.Key} {{");
                sb.Append(kv.Value.ToCss());
                sb.Append("}");
            }
            sb.Append("}");
            return sb.ToString();
        }
    }
}
