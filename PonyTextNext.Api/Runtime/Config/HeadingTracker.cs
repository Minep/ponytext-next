﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System.Collections;
using System.Collections.Generic;

namespace PonyTextNext.Api.Runtime.Config
{
    public class HeadingTracker : IEnumerable<KeyValuePair<string, int>>
    {
        private Dictionary<string, int> headings;
        private LinkedList<string> order;

        public HeadingTracker() {
            headings = new Dictionary<string, int>();
            order = new LinkedList<string>();
        }

        public IEnumerator<KeyValuePair<string, int>> GetEnumerator() {
            foreach (var k in order) {
                yield return new KeyValuePair<string, int>(k, headings[k]);
            }
        }

        public void PutHeading(int level, string identifier) {
            if (headings.TryAdd(identifier, level)) {
                order.AddLast(identifier);
            }
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
    }
}
