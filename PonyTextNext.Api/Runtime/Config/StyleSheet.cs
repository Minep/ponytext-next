﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Runtime.Registry;
using PonyTextNext.Api.Utilities;
using System.Text;

namespace PonyTextNext.Api.Runtime.Config
{
    public class StyleSheet : ICssCompatible
    {
        public StringBuilder DirectCss { get; }
        public RegistryBase<KeyValueProperties> Classes { get; }
        public RegistryBase<AtRules> AtRules { get; }

        public StyleSheet() {
            DirectCss = new StringBuilder();
            Classes = new GeneralRegistry<KeyValueProperties>();
            AtRules = new GeneralRegistry<AtRules>();
        }

        public string ToCss() {
            var sb = new StringBuilder();
            sb.Append(DirectCss);
            Classes.PopulateCssClassesTo(sb);
            foreach (var kv in AtRules) {
                sb.Append(kv.Value.ToCss());
            }
            return sb.ToString();
        }
    }
}
