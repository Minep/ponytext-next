﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document.Fragments.Terminals;

namespace PonyTextNext.Api.Runtime
{
    public interface ISLiteralEvaluator
    {
        T EvaluateAs<T>(DocumentStructuredLiteral structuredLiteral);
        void PopulateToSturcture(DocumentStructuredLiteral structuredLiteral, object structInstance);
    }
}
