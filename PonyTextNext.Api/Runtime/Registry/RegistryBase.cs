﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;

namespace PonyTextNext.Api.Runtime.Registry
{
    public abstract class RegistryBase<T> : IEnumerable<KeyValuePair<string, T>>
    {
        private Dictionary<string, T> registers;

        public RegistryBase() {
            registers = new Dictionary<string, T>();
        }

        public int Count => registers.Count;

        public void SetRegister(string name, T value = default) {
            if (!registers.TryAdd(name, value)) {
                registers[name] = value;
            }
        }

        public void CreateRegister(string name, T initialVal = default) {
            registers.TryAdd(name, initialVal);
        }

        public T ReadRegister(string name) {
            T val;
            registers.TryGetValue(name, out val);
            return val;
        }

        public bool Verify(string name, Predicate<T> validator) {
            T val;
            if (!registers.TryGetValue(name, out val)) {
                return false;
            }
            return validator.Invoke(val);
        }

        public T ReadRegisterSafe(string name) {
            T val;
            if (!registers.TryGetValue(name, out val)) {
                throw new VarNotDefinedException(name);
            }
            return val;
        }

        /// <summary>
        /// Process a value. An advanced version of ReadRegister, the <paramref name="callback"/>
        /// will be invoked iff there is a value called <paramref name="name"/>.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="callback"></param>
        public void ProcessValue(string name, Action<T> callback) {
            T val;
            if (registers.TryGetValue(name, out val)) {
                callback.Invoke(val);
            }
        }

        /// <summary>
        /// Compute a value. If the <paramref name="name"/> is found in registry, then <paramref name="func"/>
        /// will be invoked with that value as parameter. Otherwise, null will be set to the
        /// parameter. The <paramref name="func"/>'s return value will be set/add to
        /// the regsitry, will override the existing value.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="func"></param>
        public void ComputeValue(string name, Func<T, T> func) {
            T val;
            if (registers.TryGetValue(name, out val)) {
                registers[name] = func.Invoke(val);
            }
            else {
                registers.Add(name, func.Invoke(default(T)));
            }
        }

        public IEnumerator<KeyValuePair<string, T>> GetEnumerator() {
            return registers.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return registers.GetEnumerator();
        }
    }
}
