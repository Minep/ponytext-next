﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Transformer;
using System;
using System.Collections.Generic;

namespace PonyTextNext.Api.Runtime.Registry
{
    public interface ITransformerRegistry
    {
        void RegisterTransformer<T>() where T : TransformerBase;
        void RegisterTransformer(Type transformerType);
        void RegisterTransformers(IEnumerable<Type> transformers);
        void LoadFromLibrary<T>() where T : ITrasnformerLibrary;
        TransformerBase GetTransformerInstance(CallFrame callFrame);
        bool HasTransformerNamed(string name);
        IEnumerable<string> GetRegisteredTransformers();
    }
}
