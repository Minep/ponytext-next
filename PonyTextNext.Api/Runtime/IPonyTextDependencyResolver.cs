﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System.Collections.Generic;

namespace PonyTextNext.Api.Runtime
{
    public interface IPonyTextDependencyResolver
    {
        IEnumerable<string> ResolveDependencies(string dependedSource);
    }
}
