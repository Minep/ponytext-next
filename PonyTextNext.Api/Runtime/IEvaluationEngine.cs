﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.Document.Fragments.Terminals;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Registry;

namespace PonyTextNext.Api.Runtime
{
    public interface IEvaluationEngine
    {
        public PonyTextContext Context { get; }
        public ISLiteralEvaluator SLiteralEvaluator { get; }
        public RegistryBase<DocumentFragmentBase> SymbolTable { get; }
        DOMElement Invoke(CallFrame callFrame);
        DOMElement Resolve(string variableName);
        DOMElement Import(string sourceName);
        T EvaluateSLiteralAs<T>(DocumentStructuredLiteral structuredLiteral);
        DocumentFragmentBase ResolveReference(string symbolName);
        void SetValue(string variableName, DocumentFragmentBase varValue);
        void CountHeadingNumber(int level);
    }
}
