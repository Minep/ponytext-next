﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Exceptions;
using System.Collections.Generic;

namespace PonyTextNext.Api.Runtime.Invocation
{
    public class CallSignature
    {
        private List<HashSet<string>> positionals;
        private Dictionary<string, HashSet<string>> optionalNamed;
        public const string Any = "_any_";

        public CallSignature() {
            positionals = new List<HashSet<string>>();
            optionalNamed = new Dictionary<string, HashSet<string>>();
        }

        public CallSignature Positional(string type = Any, params string[] alternatives) {
            var hset = new HashSet<string>(alternatives);
            hset.Add(type);
            positionals.Add(hset);
            return this;
        }

        public CallSignature NamedOptional(string name, string type, params string[] alternatives) {
            var hset = new HashSet<string>(alternatives);
            hset.Add(type);
            if (!optionalNamed.TryAdd(name, hset)) {
                throw new PonyTextException(
                    $"Named parameter '{name}' is already added as type: '{type}'");
            }
            return this;
        }

        public void Verify(CallFrame callFrame) {
            var args = callFrame.Args;
            for (int i = 0; i < positionals.Count; i++) {
                var type = positionals[i];
                var arg = args.ReadRegister(i.ToString());
                if (arg == null) {
                    throw new InvalidFunctionInvocationException(
                        callFrame,
                        $"Expecting {i}-th parameter but found nothing.");
                }
                if (!type.Contains(Any) && !type.Contains(arg.Name)) {
                    var list = string.Join(", ", type);
                    throw new InvalidFunctionInvocationException(
                        callFrame,
                        $"The {i}-th parameter is expecting to be one of [ {list} ] but found '{arg.Name}'");
                }
            }

            foreach (var named in optionalNamed) {
                args.ProcessValue(named.Key, val => {
                    var type = named.Value;
                    if (!type.Contains(Any) && !type.Contains(val.Name)) {
                        var list = string.Join(", ", type);
                        throw new InvalidFunctionInvocationException(
                            callFrame,
                            $"The named parameter: '{named.Key}' is expecting to be one of [ {list} ] but found '{val.Name}'");
                    }
                });
            }
        }
    }
}
