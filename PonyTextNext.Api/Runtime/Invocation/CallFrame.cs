﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.Document.Fragments;
using PonyTextNext.Api.Document.Fragments.Functions;
using PonyTextNext.Api.Document.Fragments.Terminals;
using PonyTextNext.Api.Runtime.Registry;

namespace PonyTextNext.Api.Runtime.Invocation
{
    public class CallFrame
    {
        public string Target { get; }
        public RegistryBase<DocumentFragmentBase> Args { get; }

        public CallFrame(DocumentFunctionCall fnNode, IEvaluationEngine engine) {
            Target = fnNode.Target;
            Args = new GeneralRegistry<DocumentFragmentBase>();
            int counter = 0;
            foreach (DocumentFunctionArgument argument in fnNode.Children()) {
                DocumentFragmentBase arg;
                if (argument.Argument.Name.Equals(DefaultFragments.Reference)) {
                    arg = engine.ResolveReference(((DocumentMacro) argument.Argument).Symbol).DeepClone();
                }
                else {
                    arg = argument.Argument.DeepClone();
                }

                arg.Expand(engine);

                if (argument.Positional) {
                    Args.SetRegister(counter.ToString(), arg);
                    counter++;
                }
                else {
                    Args.SetRegister(argument.AnchorName, arg);
                }
            }
        }

        // evaluate the fragment into DOM before forming the call frame
        // so we can resolve the limitation mentioned in README.
        private DocumentFragmentBase expand (IEvaluationEngine engine, DocumentFragmentBase fragmentBase) {
            if (fragmentBase.Level == FragmentLevel.Terminal) {
                return fragmentBase;
            }
            var dom = fragmentBase.EvaluateToDOM(engine);
            return new DOMFragment(dom, fragmentBase.Token);
        }

        public DocumentFragmentBase GetArgument(string argName) {
            return Args.ReadRegisterSafe(argName);
        }
    }
}
