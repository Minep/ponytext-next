﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;

namespace PonyTextNext.Api.Runtime.Invocation
{
    public interface IMixinInvocator
    {
        DocumentFragmentBase ResolveMixinParam(string paramName);
        DOMElement InvokeMixin(IEvaluationEngine engine, CallFrame callFrame);
    }
}
