﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Api.Runtime
{
    public class KeyValueProperties : IEnumerable<KeyValuePair<string, string>>, ICssCompatible
    {
        private Dictionary<string, string> propertiesList;

        public KeyValueProperties() {
            propertiesList = new Dictionary<string, string>();
        }

        public KeyValueProperties(Dictionary<string, string> defaultValue) {
            propertiesList = defaultValue;
        }

        public KeyValueProperties SetProperty(string key, string value) {
            propertiesList.Add(key, value);
            return this;
        }

        public string GetProperty(string key) {
            string value;
            propertiesList.TryGetValue(key, out value);
            return value ?? string.Empty;
        }

        public T GetPropertyAs<T>(string key) {
            string val;
            if (!propertiesList.TryGetValue(key, out val)) {
                return default(T);
            }
            return (T) Convert.ChangeType(val, typeof(T));
        }

        public KeyValueProperties Union(KeyValueProperties anotherProps) {
            foreach (var entry in anotherProps) {
                propertiesList.TryAdd(entry.Key, entry.Value);
            }
            return this;
        }

        public KeyValueProperties OverrideWith(KeyValueProperties anotherProps) {
            if (anotherProps == null)
                return this;

            foreach (var entry in anotherProps) {
                if (!propertiesList.TryAdd(entry.Key, entry.Value)) {
                    propertiesList[entry.Key] = entry.Value;
                }
            }
            return this;
        }

        public int Count
        {
            get {
                return propertiesList.Count;
            }
        }

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator() {
            return propertiesList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return propertiesList.GetEnumerator();
        }

        public string ToCss() {
            var sb = new StringBuilder();
            foreach (var kv in this) {
                sb.Append($"{kv.Key}: {kv.Value};");
            }
            return sb.ToString();
        }
    }
}
