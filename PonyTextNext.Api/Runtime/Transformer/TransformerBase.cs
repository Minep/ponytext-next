﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.Document.Fragments;
using PonyTextNext.Api.Document.Fragments.Terminals;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime.Invocation;
using PonyTextNext.Api.Runtime.Registry;
using System;

namespace PonyTextNext.Api.Runtime.Transformer
{
    public abstract class TransformerBase
    {
        protected RegistryBase<DocumentFragmentBase> argList;
        protected DocumentDictionary transformerOptions;
        private CallSignature parameterSignature;
        private IEvaluationEngine bindedEngine;
        private string transformerName = "";
        public TransformerBase() {
            parameterSignature = new CallSignature();
            transformerOptions = new DocumentDictionary(Token.Empty);

            try {
                DefineSignature(parameterSignature);
                SetTransformerDefaultOptions();
            }
            catch (Exception e) {
                throw new TransformerException(transformerName, "Unexpected error at initialization.", e);
            }
        }

        public DOMElement Transform(IEvaluationEngine engine, CallFrame callFrame) {
            try {
                argList = callFrame.Args;
                this.bindedEngine = engine;
                parameterSignature.Verify(callFrame);
                engine
                    .Context
                    .TransformerOptions.ProcessValue(callFrame.Target, (val) => {
                        transformerOptions.OverrideWith(val);
                    });

                argList.ProcessValue("$option", val => {
                    if (val.Name == DefaultFragments.Dictionary) {
                        transformerOptions.OverrideWith((DocumentDictionary) val);
                    }
                });

                var css = GetArgumentAs<KeyValueProperties>("$css", null);
                var attr = GetArgumentAs<KeyValueProperties>("$attrs", null);
                var result = Invoked(engine);
                if (result != null && result.Type <= DOMType.Inline) {
                    ((DOMRoot) result).InlineStyle.OverrideWith(css);
                    ((DOMRoot) result).Attributes.OverrideWith(attr);
                }
                return result;
            }
            catch (Exception e) {
                throw new TransformerException(transformerName, e.Message, e);
            }
        }

        protected DocumentFragmentBase AcquireArgument(string name) {
            return argList.ReadRegisterSafe(name);
        }

        protected DocumentFragmentBase AcquireArgument(int index) {
            return AcquireArgument(index.ToString());
        }

        protected DocumentFragmentBase AcquireArgumentAsFragment(int index, string fragmentName) {
            return AcquireArgumentAsFragment(index.ToString(), fragmentName);
        }
        protected DocumentFragmentBase AcquireArgumentAsFragment(string name, string fragmentName) {
            var arg = AcquireArgument(name);
            if (arg.Name.Equals(fragmentName)) {
                return arg;
            }
            throw new ArgumentException($"Unable to get '{arg.Name}' as '{fragmentName}'");
        }

        protected DocumentFragmentBase GetArgumentOrElse(string name, DocumentFragmentBase otherwise) {
            return argList.ReadRegister(name) ?? otherwise;
        }

        /// <summary>
        /// Get an argument as CLR primitive type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index"></param>
        /// <returns></returns>
        protected T AcquireArgumentAs<T>(int index) {
            return AcquireArgumentAs<T>(index.ToString());
        }

        /// <summary>
        /// Acquire an argument as CLR primitive type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        protected T AcquireArgumentAs<T>(string name) {
            var arg = AcquireArgument(name);

            return convertFragmentToCLR<T>(name, arg);
        }

        /// <summary>
        /// Acquire an argument as CLR primitive type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index"></param>
        /// <returns></returns>
        protected T GetArgumentAs<T>(int index, T alternative = default(T)) {
            return GetArgumentAs<T>(index.ToString(), alternative);
        }

        /// <summary>
        /// Get an argument as CLR primitive type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        protected T GetArgumentAs<T>(string name, T alternative = default(T)) {
            var arg = argList.ReadRegister(name);
            if (arg == null) {
                return alternative;
            }

            return convertFragmentToCLR<T>(name, arg);
        }

        protected T GetFieldAs<T>(DocumentDictionary dict, string path, T alternative = default(T)) {
            var arg = dict.IndexingBy(path);
            if (arg == null) {
                return alternative;
            }

            return convertFragmentToCLR<T>(path, arg);
        }

        protected bool ArgumentHasType(int index, string desireType) {
            return ArgumentHasType(index.ToString(), desireType);
        }

        protected bool ArgumentHasType(string name, string desireType) {
            return AcquireArgument(name).Name.Equals(desireType);
        }

        protected abstract DOMElement Invoked(IEvaluationEngine engine);

        protected virtual void DefineSignature(CallSignature parameterSignature) {

        }

        protected virtual void SetTransformerDefaultOptions() {

        }

        private T convertFragmentToCLR<T>(string name, DocumentFragmentBase arg) {
            var type = typeof(T);
            if (type.IsArray && arg.WithName(DefaultFragments.Array)) {
                var elem = type.GetElementType();
                var arr_arg = (DocumentTypeArray) arg;
                var arr = Array.CreateInstance(elem, arr_arg.Count);
                try {
                    for (int i = 0; i < arr_arg.Count; i++) {
                        arr.SetValue(convertFragmentToPrimitive(elem, arr_arg[i]), i);
                    }
                    return (T) (object) arr;
                }
                catch (Exception e) {
                    throw new PonyTextException(
                        $"Mismatch array element type '{elem.Name}' when proccessing array valued parameter of '{name}'", e);
                }
            }
            if (arg.WithName(DefaultFragments.SLiteral)) {
                return bindedEngine.SLiteralEvaluator.EvaluateAs<T>((DocumentStructuredLiteral) arg);
            }
            return (T) convertFragmentToPrimitive(type, (DocumentTerminal) arg);
        }

        private object convertFragmentToPrimitive(Type type, DocumentFragmentBase arg) {
            if (!(arg is DocumentTerminal)) {
                throw new ArgumentException($"This fragment with type '{arg.Name}' can not be converted to CLR type.");
            }
            var argT = (DocumentTerminal) arg;
            try {
                return Convert.ChangeType(argT.AsString(), type);
            }
            catch (Exception e) {
                throw new ArgumentException(
                    $"Terminal with type: '{arg.Name}' can not be converted to '{type.Name}'", e);
            }
        }
    }
}
