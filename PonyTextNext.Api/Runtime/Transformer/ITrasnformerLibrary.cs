﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;
using System.Collections.Generic;

namespace PonyTextNext.Api.Runtime.Transformer
{
    public interface ITrasnformerLibrary
    {
        IEnumerable<Type> GetAllTransformers();
    }
}
