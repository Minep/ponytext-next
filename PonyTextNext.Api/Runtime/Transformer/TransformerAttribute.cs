﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;

namespace PonyTextNext.Api.Runtime.Transformer
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class TransformerAttribute : Attribute
    {
        public string Name { get; }
        public TransformerAttribute(string name) {
            Name = name;
        }
    }
}
