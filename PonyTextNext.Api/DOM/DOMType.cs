﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.DOM
{
    public enum DOMType : int
    {
        Root = 0,
        Inline = 1,
        Void = 2,
        Leaf = 2
    }
}
