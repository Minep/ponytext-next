﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM.Control;
using PonyTextNext.Api.DOM.ParagraphLevel;

namespace PonyTextNext.Api.DOM
{
    public abstract class DOMVisitorBase
    {
        public virtual void visitRoot(DOMRoot rootElement) {
            foreach (var item in rootElement.Children()) {
                item.Accept(this);
            }
        }
        public virtual void visitInline(DOMInline inlineElement) {
            foreach (var item in inlineElement.Children()) {
                item.Accept(this);
            }
        }
        public virtual void visitParagraph(DOMParagraph inlineElement) {
            foreach (var item in inlineElement.Children()) {
                item.Accept(this);
            }
        }
        public virtual void visitHeading(DOMHeading inlineElement) {
            foreach (var item in inlineElement.Children()) {
                item.Accept(this);
            }
        }
        public virtual void visitLeaf(DOMLeaf leafElement) {

        }

        public virtual void visitSpacingContol(DOMSpacing fillControl) {

        }
    }
}
