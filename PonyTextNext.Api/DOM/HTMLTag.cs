﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.DOM
{
    public class HTMLTag
    {
        public const string Paragraph = "p";
        public const string Division = "div";
        public const string Span = "span";
        public const string Strong = "strong";
        public const string Italic = "i";
        public const string Mark = "mark";
        public const string Break = "br";
        public const string List = "ul";
        public const string ListOrdered = "ol";
        public const string ListItem = "li";

        public static readonly string[] Headings
            = new string[] { "h1", "h2", "h3", "h4", "h5", "h6" };
    }
}
