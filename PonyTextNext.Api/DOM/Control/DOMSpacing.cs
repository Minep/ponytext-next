﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Api.DOM.Control
{
    public class DOMSpacing : DOMElement
    {
        public const int ORIENTATION_VERTICAL = 0;
        public const int ORIENTATION_HORIZONTAL = 1;

        public int Orientation { get; }
        public string Size { get; }
        public DOMSpacing(int orientation, string Unit) : base(DOMType.Leaf) {
            Orientation = orientation;
            Size = Unit;
        }

        public override void Accept(DOMVisitorBase visitorBase) {
            visitorBase.visitSpacingContol(this);
        }
    }
}
