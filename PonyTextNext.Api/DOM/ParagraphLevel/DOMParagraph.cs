﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.DOM.ParagraphLevel
{
    public class DOMParagraph : DOMRoot
    {
        public DOMParagraph(string tag) : base(tag) {
        }

        public override void Accept(DOMVisitorBase visitorBase) {
            visitorBase.visitParagraph(this);
        }

        public override void AddToChildren(DOMElement newChild) {
            if (newChild == null) {
                return;
            }
            if (newChild.Type >= DOMType.Inline) {
                base.AddToChildren(newChild);
            }
            else {
                base.AddToSiblings(newChild);
            }
        }
    }
}
