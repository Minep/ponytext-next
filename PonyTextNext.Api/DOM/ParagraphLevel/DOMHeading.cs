﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.DOM.ParagraphLevel
{
    public class DOMHeading : DOMParagraph
    {
        public int Level { get; }
        public DOMHeading(int level) : base(HTMLTag.Headings[level]) {
            Level = level;
        }

        public override void Accept(DOMVisitorBase visitorBase) {
            visitorBase.visitHeading(this);
        }
    }
}
