﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System.Collections.Generic;

namespace PonyTextNext.Api.DOM.ParagraphLevel
{
    public class DOMInline : DOMRoot
    {
        public DOMInline(string tag) : base(tag, DOMType.Inline) {

        }

        public override void AddToChildren(DOMElement newChild) {
            if (newChild == null) return;
            if (newChild.Type > Type) {
                base.AddToChildren(newChild);
            }
        }

        public override ICollection<DOMElement> FlattenToList() {
            ToOrphan();
            return new List<DOMElement>() { this };
        }

        public override void Accept(DOMVisitorBase visitorBase) {
            visitorBase.visitInline(this);
        }
    }
}
