﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Api.DOM
{
    public class DOMEmpty : DOMElement
    {
        public DOMEmpty() : base(DOMType.Leaf) {
        }

        public override void Accept(DOMVisitorBase visitorBase) {
            // do nothing
        }
    }
}
