﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Utilities;

namespace PonyTextNext.Api.DOM
{
    public abstract class DOMElement : NTreeRoot<DOMElement>
    {
        public const DOMElement None = null;
        public DOMType Type { get; }

        public DOMElement(DOMType type) {
            Type = type;
        }

        public override void AddToChildren(DOMElement newChild) {
            if (newChild == null || newChild is DOMEmpty) {
                return;
            }
            base.AddToChildren(newChild);
        }

        public override void AddToSiblings(DOMElement newSibling) {
            if (newSibling == null || newSibling is DOMEmpty) {
                return;
            }
            //if (PrevSibling != null && PrevSibling.Type == DOMType.Leaf && newSibling.Type == DOMType.Leaf) {
            //    (PrevSibling as DOMLeaf).MergeFrom((newSibling as DOMLeaf).Content);
            //}
            //else {
            //    base.AddToSiblings(newSibling);
            //}
            base.AddToSiblings(newSibling);
        }

        /// <summary>
        /// Check if this tag is ignorable.
        /// This is usually empty, no inline styles or attributes
        /// associated to it.
        /// <para>
        /// Normally, PonyText will preserve all these kind of tag. This method 
        /// is useful to renderer for deciding which tag to be rendered.
        /// </para>
        /// </summary>
        /// <returns></returns>
        public virtual bool IgnorableTag() {
            return !HasChildren;
        }
        public abstract void Accept(DOMVisitorBase visitorBase);

        /// <summary>
        /// Change the root node of the tree.
        /// If it is a leaf, then create a root.
        /// 
        /// This operation assume the current node represent the root of non-subtree
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        public DOMElement withRoot(DOMElement root) {
            if (Type == DOMType.Leaf) {
                root.AddToChildren(this);
            }
            else {
                root.AddToChildren(this.DirectSuccessor);
            }
            return root;
        }

        public static DOMElement getEmpty () {
            return new DOMEmpty();
        }
    }
}
