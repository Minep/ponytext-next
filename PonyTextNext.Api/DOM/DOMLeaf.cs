﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;
using System.Collections.Generic;
using System.Net;

namespace PonyTextNext.Api.DOM
{
    public class DOMLeaf : DOMElement
    {
        public string Content { get; private set; }
        public DOMLeaf(string content, bool sanitizeHTML = true) : base(DOMType.Leaf) {
            this.Content = sanitizeHTML
                ? WebUtility.HtmlEncode(content)
                    : content;
        }

        public void MergeFrom(string content) {
            this.Content += content;
        }

        public override void AddToChildren(DOMElement fragmentBase) {
            throw new InvalidOperationException();
        }

        public override ICollection<DOMElement> FlattenToList() {
            ToOrphan();
            return new List<DOMElement>() { this };
        }

        public override bool IgnorableTag() {
            return string.IsNullOrEmpty(Content);
        }

        public override void Accept(DOMVisitorBase visitorBase) {
            visitorBase.visitLeaf(this);
        }

        public static DOMLeaf Create(string content) {
            return new DOMLeaf(content);
        }
    }
}
