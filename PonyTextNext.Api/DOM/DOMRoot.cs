﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Runtime;
using System.Collections.Generic;

namespace PonyTextNext.Api.DOM
{
    public class DOMRoot : DOMElement
    {
        public string Tag { get; }
        public List<string> StyleClasses { get; private set; }
        public KeyValueProperties InlineStyle { get; private set; }
        public KeyValueProperties Attributes { get; private set; }

        public DOMRoot(string tag, params DOMElement[] children) : this(tag, DOMType.Root) {
            AddToChildrenRanged(children);
        }

        public DOMRoot(string tag, DOMType type) : base(type) {
            Tag = tag;
            StyleClasses = new List<string>();
            InlineStyle = new KeyValueProperties();
            Attributes = new KeyValueProperties();
        }

        public override bool IgnorableTag() {
            return base.IgnorableTag()
                    && InlineStyle.Count == 0
                    && Attributes.Count == 0;
        }
        public override void Accept(DOMVisitorBase visitorBase) {
            visitorBase.visitRoot(this);
        }

        /// <summary>
        /// A handy method if you just want something to group all your children
        /// </summary>
        /// <returns></returns>
        public static DOMRoot GetRoot(string tag) {
            return new DOMRoot(tag);
        }
    }
}
