﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;

namespace PonyTextNext.Api.DOM
{
    public class DOMVoid : DOMRoot
    {
        public DOMVoid(string tag) : base(tag, DOMType.Void) {

        }

        public override bool IgnorableTag() {
            return false;
        }

        public override void AddToChildren(DOMElement newChild) {
            throw new InvalidOperationException("A void element can not have children");
        }
    }
}
