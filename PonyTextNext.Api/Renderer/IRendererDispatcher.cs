﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using System;
using System.IO;

namespace PonyTextNext.Api.Renderer
{
    public interface IRendererDispatcher
    {
        void RegisterRenderer(string rendererName, Type renderer);
        void LoadFromLibrary<T>() where T : IRendererLibrary;
        void RenderToFile(string renderer, DOMElement element, string file);
        void RenderToStream(string renderer, DOMElement element, Stream stream);
    }
}
