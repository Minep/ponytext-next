﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;

namespace PonyTextNext.Api.Renderer
{
    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class RendererAttribute : Attribute
    {
        public string Name { get; }

        public RendererAttribute(string name) {
            Name = name;
        }
    }
}
