﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using System.IO;

namespace PonyTextNext.Api.Renderer
{
    public abstract class RendererBase
    {
        protected DOMElement DOM;
        protected PonyTextContext setting;
        public DocumentStatistic Statistic { get; protected set; }

        protected RendererBase(DOMElement DOM, PonyTextContext setting) {
            this.DOM = DOM;
            this.setting = setting;
        }

        public abstract MemoryStream Render();
    }
}
