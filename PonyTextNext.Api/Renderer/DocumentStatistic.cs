﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.Renderer
{
    public class DocumentStatistic
    {
        public int ParagraphCount { get; private set; } = 0;
        public int CharacterCount { get; private set; } = 0;
        public int CharacterNonWSCount { get; private set; } = 0;

        public void CountCharacter(string contentFragment) {
            CharacterCount += contentFragment.Length;
            int nonws = 0;
            for (int i = 0; i < contentFragment.Length; i++) {
                if (!char.IsWhiteSpace(contentFragment[i])) {
                    nonws++;
                }
            }
            CharacterNonWSCount += nonws;
        }

        public void CountParagraph(int delta = 1) {
            ParagraphCount += delta;
        }

        public override string ToString() {
            return
                $"Summary: \n" +
                $"\tParagraph: {ParagraphCount}\n" +
                $"\tCharacter (with white-space): {CharacterCount}\n" +
                $"\tCharacter (without whit-space): {CharacterNonWSCount}";
        }
    }
}
