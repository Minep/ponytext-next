﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;
using System.Collections.Generic;

namespace PonyTextNext.Api.Renderer
{
    public interface IRendererLibrary
    {
        IEnumerable<Type> GetAllRenderer();
    }
}
