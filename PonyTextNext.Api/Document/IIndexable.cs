﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Api.Document
{
    public interface IIndexable
    {
        DocumentFragmentBase IndexingBy(string index);
        void AssignWith(string index, DocumentFragmentBase value);
    }
}
