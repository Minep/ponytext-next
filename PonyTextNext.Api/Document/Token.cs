﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.Document
{
    public class Token
    {
        public static readonly Token Empty = new Token(string.Empty, -1, -1);
        public string Lexeme { get; }
        public int Row { get; }
        public int Column { get; }

        public Token(string lexeme, int row, int column) {
            Lexeme = lexeme;
            Row = row;
            Column = column;
        }

        public override string ToString() {
            return $"'{Lexeme}' at column {Column} of row {Row}.";
        }
    }
}
