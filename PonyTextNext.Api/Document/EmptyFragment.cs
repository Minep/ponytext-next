﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using System;

namespace PonyTextNext.Api.Document
{
    public class EmptyFragment : DocumentFragmentBase
    {
        public EmptyFragment()
            : base("empty", FragmentLevel.Terminal, Token.Empty) {
        }

        public override void AddToChildren(DocumentFragmentBase fragmentBase) {
            throw new InvalidOperationException();
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            return DOMElement.getEmpty();
        }

        public override DocumentFragmentBase DeepClone() {
            return this;
        }

        public override string GetText() {
            return "";
        }
    }
}
