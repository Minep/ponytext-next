﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.Document
{
    public class DefaultFragments
    {
        public const string Paragraph = "paragraph";
        public const string StringTerminal = "str";
        public const string NumberTerminal = "num";
        public const string Container = "container";
        public const string Indexer = "index";

        public const string Invocation = "invoke";
        public const string Arguments = "arg";
        public const string Array = "array";
        public const string SLiteral = "sliteral";
        public const string Reference = "ref";
        public const string Assignment = "assign";
        public const string Injection = "inject";
        public const string Dictionary = "dictionary";
        public const string BooleanTerminal = "bool";

        public static readonly EmptyFragment EmptyFragment = new EmptyFragment();
    }
}
