﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Runtime;
using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Api.Document
{
    public interface IAssignable
    {
        void Assign(IEvaluationEngine engine, DocumentFragmentBase value);
    }
}
