﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Utilities;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Api.Document
{
    public abstract class DocumentFragmentBase : NTreeRoot<DocumentFragmentBase>
    {
        public string Name { get; }

        public FragmentLevel Level { get; }

        public Token Token { get; }

        protected DocumentFragmentBase(string name, FragmentLevel level, Token token) {
            Name = name;
            Level = level;
            Token = token;
        }

        public override void AddToChildren(DocumentFragmentBase fragmentBase) {
            if (fragmentBase == null) {
                return;
            }

            if (Level != FragmentLevel.Invocation && fragmentBase.Level >= this.Level) {
                base.AddToSiblings(fragmentBase);
            }
            else {
                base.AddToChildren(fragmentBase);
            }
        }

        public override void AddToSiblings(DocumentFragmentBase newSibling) {
            if (newSibling == null) {
                return;
            }
            base.AddToSiblings(newSibling);
        }

        public virtual string GetText() {
            var sb = new StringBuilder();

            sb.Append($"<{Name} level=\"{Level}\">");
            foreach (var child in this.Children()) {
                sb.Append(child.GetText());
            }
            sb.Append($"</{Name}>");

            return sb.ToString();
        }

        public virtual DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            var dom = new DOMRoot(Name);
            foreach (var subtree in Children()) {
                dom.AddToChildren(subtree.EvaluateToDOM(engine));
            }
            return dom;
        }

        /// <summary>
        /// Get expansion on the node, if applicable.
        /// </summary>
        /// <returns></returns>
        public virtual DocumentFragmentBase GetExpansion(IEvaluationEngine engine) {
            return null;
        }

        /// <summary>
        /// Perform breadth first in-place subtree expansion
        /// </summary>
        public void Expand(IEvaluationEngine engine) {
            var queue = new Queue<DocumentFragmentBase>();
            queue.Enqueue(this);
            while (queue.Count > 0) {
                var node = queue.Dequeue();
                foreach (var child in node.Children()) {
                    var expansion = child.GetExpansion(engine);
                    if (expansion == null) {
                        // No expansion found, continue searching
                        queue.Enqueue(child);
                    }
                    else {
                        // expansion found
                        child.ReplaceWith(expansion);

                        // we wish to expand further in this new subtree
                        queue.Enqueue(expansion);
                    }
                }
            }
        }

        /// <summary>
        /// You must call this method to deep clone the vDOM tree 
        /// before any manipulations.
        /// Otherwise you may encounter unexpected side effect.
        /// </summary>
        /// <returns></returns>
        public abstract DocumentFragmentBase DeepClone();

        public bool WithName(string name) {
            return Name.Equals(name);
        }

        public bool WithLevel(FragmentLevel level) {
            return Level == level;
        }
    }
}
