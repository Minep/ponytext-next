﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.Document
{
    public enum FragmentLevel : int
    {
        Invocation = -1,
        Terminal = 0,
        Phrasing = 1,
        Paragraph = 2,
        Block = 3
    }
}
