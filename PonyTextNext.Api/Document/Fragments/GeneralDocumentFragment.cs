﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.Document.Fragments
{
    public class GeneralDocumentFragment : DocumentFragmentBase
    {
        public GeneralDocumentFragment(string fragmentName, Token token) : base(fragmentName, FragmentLevel.Block, token) {
            // nothing here, just make DocumentFragmentBase instanciable
        }

        public override DocumentFragmentBase DeepClone() {
            var obj = new GeneralDocumentFragment(Name, Token);
            foreach (var subtree in Children()) {
                obj.AddToChildren(subtree.DeepClone());
            }
            return obj;
        }
    }
}
