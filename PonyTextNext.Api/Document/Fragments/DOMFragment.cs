﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Api.Document.Fragments
{
    public class DOMFragment : DocumentFragmentBase
    {   
        private DOMElement dom { get; }
        public DOMFragment(DOMElement dom,Token token) : base("$dom", FragmentLevel.Terminal, token) {
            this.dom = dom;
        }

        public override void AddToChildren(DocumentFragmentBase fragmentBase) {
            throw new InvalidOperationException();
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            return dom;
        }

        public override DocumentFragmentBase DeepClone() {
            return this;
        }
    }
}
