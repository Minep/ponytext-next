﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using System.Collections.Generic;

namespace PonyTextNext.Api.Document.Fragments.Terminals
{
    public class DocumentTypeArray : DocumentFragmentBase, IIndexable
    {
        private List<DocumentFragmentBase> elements;
        public DocumentTypeArray()
            : base(DefaultFragments.Array, FragmentLevel.Terminal, Token.Empty) {
            elements = new List<DocumentFragmentBase>();
        }

        private DocumentTypeArray(List<DocumentFragmentBase> initalContent, Token token)
            : base(DefaultFragments.Array, FragmentLevel.Terminal, token) {
            elements = initalContent;
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            return null;
        }

        public override void AddToChildren(DocumentFragmentBase fragmentBase) {
            elements.Add(fragmentBase);
        }

        public override IEnumerable<DocumentFragmentBase> Children() {
            return elements;
        }

        public DocumentFragmentBase this[int index]
        {
            get {
                return elements[index];
            }
        }

        public int Count => elements.Count;

        public override DocumentFragmentBase DeepClone() {
            var obj = new DocumentTypeArray();
            foreach (var subtree in Children()) {
                obj.AddToChildren(subtree.DeepClone());
            }
            return obj;
        }

        public DocumentFragmentBase IndexingBy(string index) {
            int i;
            if (int.TryParse(index, out i)) {
                return elements[i];
            }
            throw new PonyTextException(
                $"The value '{index}' can not be used to index"
            );
        }

        public void AssignWith(string index, DocumentFragmentBase value) {
            int i;
            if (int.TryParse(index, out i)) {
                elements[i] = value;
            }
            throw new PonyTextException(
                $"The value '{index}' can not be used to index"
            );
        }
    }
}
