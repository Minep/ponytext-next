﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using System;
using System.Text;

namespace PonyTextNext.Api.Document.Fragments.Terminals
{
    public class DocumentVariableAssignment : DocumentFragmentBase
    {
        public DocumentFragmentBase target { get; }
        public DocumentFragmentBase value { get; }
        public DocumentVariableAssignment(DocumentFragmentBase target, DocumentFragmentBase value)
            : base(DefaultFragments.Assignment, FragmentLevel.Terminal, Token.Empty) {
            this.target = target;
            this.value = value;
        }

        public override string GetText() {
            var sb = new StringBuilder();
            sb.Append($"<{Name}>");
            sb.Append("<Target>");
            sb.Append(target.GetText());
            sb.Append("</Target>");
            sb.Append("<Value>");
            sb.Append(value.GetText());
            sb.Append("</Value>");
            sb.Append($"</{Name}>");
            return sb.ToString();
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            try {
                var assignable = target as IAssignable;
                if (assignable == null) {
                    throw new PonyTextException($"I can't make an assignment to '{target.Name}'");
                }
                assignable.Assign(engine, value);
                return DOMElement.None;
            }
            catch (Exception e) {
                throw new PonyTextException($"Near {Token}.", e);
            }
        }

        public override DocumentFragmentBase DeepClone() {
            return new DocumentVariableAssignment(target, value.DeepClone());
        }
    }
}
