﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using System;

namespace PonyTextNext.Api.Document.Fragments.Terminals
{
    public class DocumentMacro : DocumentFragmentBase, IAssignable
    {
        public string Symbol { get; }
        public DocumentMacro(string symbolName, Token token)
            : base(DefaultFragments.Reference, FragmentLevel.Terminal, token) {
            Symbol = symbolName;
        }

        public override string GetText() {
            return $"<{Name} symbol=\"{Symbol}\"/>";
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            try {
                return engine.Resolve(Symbol);
            }
            catch (Exception e) {
                throw new PonyTextException($"Near {Token}.", e);
            }
        }

        public override DocumentFragmentBase GetExpansion(IEvaluationEngine engine) {
            var val = engine.ResolveReference(Symbol).DeepClone();
            return val;
        }

        public override DocumentFragmentBase DeepClone() {
            return new DocumentMacro(Symbol, Token);
        }

        public void Assign(IEvaluationEngine engine, DocumentFragmentBase value) {
            engine.SetValue(Symbol, value);
        }
    }
}
