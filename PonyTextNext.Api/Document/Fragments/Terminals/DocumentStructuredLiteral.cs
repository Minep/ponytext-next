﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using System;
using System.Text;

namespace PonyTextNext.Api.Document.Fragments.Terminals
{
    public class DocumentStructuredLiteral : DocumentFragmentBase
    {
        public string TargetStructure { get; }
        public string JsonData { get; }

        public DocumentStructuredLiteral(string structureName, string json)
            : base(DefaultFragments.SLiteral, FragmentLevel.Terminal, Token.Empty) {
            TargetStructure = structureName;
            JsonData = json;
        }

        public override void AddToChildren(DocumentFragmentBase fragmentBase) {
            throw new InvalidOperationException();
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            return DOMLeaf.Create(GetText());
        }

        public override string GetText() {
            var sb = new StringBuilder();

            sb.Append($"<{Name} level=\"{Level}\" target=\"{TargetStructure}\">");
            sb.Append(JsonData);
            sb.Append($"</{Name}>");

            return sb.ToString();
        }

        public override DocumentFragmentBase DeepClone() {
            return new DocumentStructuredLiteral(TargetStructure, JsonData);
        }
    }
}
