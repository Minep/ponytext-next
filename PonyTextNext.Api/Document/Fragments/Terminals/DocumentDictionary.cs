﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Api.Document.Fragments.Terminals
{
    public class DocumentDictionary : DocumentFragmentBase, IIndexable
    {
        public Dictionary<string, DocumentFragmentBase> Fields { get; }
        public DocumentDictionary(Token token) 
            : base(DefaultFragments.Dictionary, FragmentLevel.Terminal, token) {
            Fields = new Dictionary<string, DocumentFragmentBase>();
        }

        public void AppendFields(string name, DocumentFragmentBase value) {
            if (!Fields.TryAdd(name, value)) {
                throw new PonyTextException($"Duplicated field: '{name}' ({Token})");
            }
        }

        public DocumentFragmentBase GetValue(string name) {
            DocumentFragmentBase value;
            if (!Fields.TryGetValue(name, out value)) {
                return null;
            }
            return value;
        }

        // '/' separated path
        public DocumentFragmentBase IndexingBy(string path) {
            if (string.IsNullOrWhiteSpace(path)) {
                throw new PonyTextException($"Path can not be null, empty or whitespaced");
            }
            string[] components = path.Split('/');
            DocumentFragmentBase currentLevel = this;
            foreach (var item in components) {
                if (!(currentLevel is DocumentDictionary)) {
                    throw new PonyTextException($"I can't access the non-struct type.");
                }
                currentLevel = ((DocumentDictionary) currentLevel).GetValue(item);
            }
            return currentLevel;
        }

        public void AssignWith(string path, DocumentFragmentBase fragmentBase) {
            if (string.IsNullOrWhiteSpace(path)) {
                throw new PonyTextException($"Path can not be null, empty or whitespaced");
            }
            string[] components = path.Split('/');
            DocumentFragmentBase currentLevel = this;
            for(int i=0; i < components.Length - 1; i++) {
                if (!(currentLevel is DocumentDictionary)) {
                    throw new PonyTextException($"I can't access the non-dictionary type.");
                }
                currentLevel = ((DocumentDictionary) currentLevel).GetValue(components[i]);
            }
            var key = components[components.Length - 1];
            if (currentLevel is DocumentDictionary) {
                var dict = ((DocumentDictionary) currentLevel);
                if (!dict.Fields.TryAdd(key, fragmentBase)) {
                    dict.Fields[key] = fragmentBase;
                }
            }
            else {
                throw new PonyTextException($"Non-dictionary type doesn't have key named '{key}'");
            }
        }

        internal void OverrideWith(DocumentDictionary val) {
            foreach(var kvpair in val.Fields) {
                if (Fields.ContainsKey(kvpair.Key)) {
                    Fields[kvpair.Key] = kvpair.Value;
                }
            }
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            return DOMLeaf.Create($"<PonyTextDictionary@{GetHashCode():X}>");
        }

        public override DocumentFragmentBase DeepClone() {
            var dstruct = new DocumentDictionary(Token);
            foreach (var kvpair in Fields) {
                dstruct.Fields.Add(kvpair.Key, kvpair.Value.DeepClone());
            }
            return dstruct;
        }
    }
}
