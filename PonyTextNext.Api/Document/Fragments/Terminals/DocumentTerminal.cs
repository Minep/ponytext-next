﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using System;
using System.Text;

namespace PonyTextNext.Api.Document.Fragments
{
    public class DocumentTerminal : DocumentFragmentBase
    {
        private string content;
        public DocumentTerminal(string content, string termType, Token token)
            : base(termType, FragmentLevel.Terminal, token) {
            this.content = content;
        }

        public decimal AsDecimal() {
            if (this.Name == DefaultFragments.NumberTerminal) {
                return decimal.Parse(content);
            }
            throw new InvalidCastException();
        }

        public string AsString() {
            return content;
        }

        public override void AddToChildren(DocumentFragmentBase fragmentBase) {
            throw new InvalidOperationException();
        }

        public override string GetText() {
            var sb = new StringBuilder();

            sb.Append($"<{Name} level=\"{Level}\">");
            sb.Append(content);
            sb.Append($"</{Name}>");

            return sb.ToString();
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            return DOMLeaf.Create(content);
        }

        public override DocumentFragmentBase DeepClone() {
            return new DocumentTerminal(content, Name, Token);
        }

        public static DocumentFragmentBase OfString(string content) {
            return new DocumentTerminal(content, DefaultFragments.StringTerminal, Token.Empty);
        }
    }
}
