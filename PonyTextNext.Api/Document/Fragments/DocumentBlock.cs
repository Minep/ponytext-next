﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;

namespace PonyTextNext.Api.Document.Fragments
{
    public class DocumentBlock : DocumentFragmentBase
    {
        public DocumentBlock(string blockType)
            : base(blockType, FragmentLevel.Block, Token.Empty) {

        }

        public override DocumentFragmentBase DeepClone() {
            var obj = new DocumentBlock(Name);
            foreach (var subtree in Children()) {
                obj.AddToChildren(subtree.DeepClone());
            }
            return obj;
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            var dom = new DOMRoot(HTMLTag.Division);
            foreach (var subtree in Children()) {
                dom.AddToChildren(subtree.EvaluateToDOM(engine));
            }
            return dom;
        }
    }
}
