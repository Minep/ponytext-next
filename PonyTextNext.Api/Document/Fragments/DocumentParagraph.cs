﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.DOM.ParagraphLevel;
using PonyTextNext.Api.Runtime;

namespace PonyTextNext.Api.Document.Fragments
{
    public class DocumentParagraph : DocumentFragmentBase
    {
        public DocumentParagraph()
            : base(DefaultFragments.Paragraph, FragmentLevel.Paragraph, Token.Empty) {

        }

        public DocumentParagraph(string defaultContent) : this() {
            base.AddToChildren(new DocumentTerminal(defaultContent, DefaultFragments.StringTerminal, Token));
        }

        public void AddTerminal(string terminalString) {
            base.AddToChildren(new DocumentTerminal(terminalString, DefaultFragments.StringTerminal, Token));
        }

        public override DocumentFragmentBase DeepClone() {
            var obj = new DocumentParagraph();
            foreach (var subtree in Children()) {
                obj.AddToChildren(subtree.DeepClone());
            }
            return obj;
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            var dom = new DOMParagraph(HTMLTag.Paragraph);
            dom.StyleClasses.Add("text-paragraph");
            foreach (var subtree in Children()) {
                dom.AddToChildren(subtree.EvaluateToDOM(engine));
            }
            return dom;
        }
    }
}
