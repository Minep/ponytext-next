﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;

namespace PonyTextNext.Api.Document.Fragments
{
    public class DocumentIndexer : DocumentFragmentBase, IAssignable
    {
        public string IndexedID { get; }
        public DocumentFragmentBase Index { get; }
        public DocumentIndexer(string id, DocumentFragmentBase index,Token token) 
            : base(DefaultFragments.Indexer, FragmentLevel.Terminal, token) {
            IndexedID = id;
            Index = index;
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            IIndexable indexable;
            string key;
            PopulateIndexableAndKey(engine, out indexable, out key);

            var indexed = indexable.IndexingBy(key);
            if (indexed == null) {
                throw new PonyTextException($"I can't find things in '{key}'", Token);
            }
            return indexed.EvaluateToDOM(engine);
        }

        public override DocumentFragmentBase DeepClone() {
            return new DocumentIndexer(IndexedID, Index.DeepClone(), Token);
        }

        public void Assign(IEvaluationEngine engine, DocumentFragmentBase value) {
            IIndexable indexable;
            string key;
            PopulateIndexableAndKey(engine, out indexable, out key);

            indexable.AssignWith(key, value);
        }

        private void PopulateIndexableAndKey(IEvaluationEngine engine, out IIndexable indexable, out string key) {
            var id = engine.ResolveReference(IndexedID) as IIndexable;
            if (id == null) {
                throw new PonyTextException(
                    $"I can't index non-indexable data type (e.g., you can only " +
                    $"index array and dictionary)", Token);
            }

            var index = Index.EvaluateToDOM(engine);
            if (index is DOMLeaf) {
                key = ((DOMLeaf) index).Content;
                indexable = id;
                return;
            }
            throw new PonyTextException($"The index must be primitive (i.e., string or number)", Token);
        }
    }
}
