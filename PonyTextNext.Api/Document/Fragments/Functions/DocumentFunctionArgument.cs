﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document.Fragments.Terminals;
using PonyTextNext.Api.Runtime;
using System.Text;

namespace PonyTextNext.Api.Document.Fragments.Functions
{
    public class DocumentFunctionArgument : DocumentFragmentBase {
        public DocumentFragmentBase Argument { get; private set; }
        public string AnchorName { get; private set; }
        public bool Positional
        {
            get {
                return AnchorName == string.Empty;
            }
        }

        public DocumentFunctionArgument(DocumentFragmentBase argVal, string anchorName = "")
            : base(DefaultFragments.Arguments, FragmentLevel.Terminal, Token.Empty) {
            Argument = argVal;
            AnchorName = anchorName;
        }

        public override DocumentFragmentBase GetExpansion(IEvaluationEngine engine) {
            if (Argument.Level <= FragmentLevel.Terminal && Argument.Name == DefaultFragments.Reference) {
                var arg = ((DocumentMacro) Argument).Symbol;
                Argument = engine.ResolveReference(arg).DeepClone();
            }
            Argument.Expand(engine);
            return null;
        }

        public override string GetText() {
            var sb = new StringBuilder();

            sb.Append($"<{Name} level=\"{Level}\" anchor=\"{AnchorName}\">");
            sb.Append(Argument.GetText());
            sb.Append($"</{Name}>");

            return sb.ToString();
        }

        public override DocumentFragmentBase DeepClone() {
            return new DocumentFunctionArgument(Argument.DeepClone(), AnchorName);
        }
    }
}
