﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using System;

namespace PonyTextNext.Api.Document.Fragments.Functions
{
    public class DocumentInjectionPoint : DocumentFragmentBase
    {
        public string SourceFile { get; }
        public DocumentInjectionPoint(string source, Token token)
            : base(DefaultFragments.Injection, FragmentLevel.Terminal, token) {
            SourceFile = source;
        }

        public override string GetText() {
            return $"<{Name} file=\"{SourceFile}\"/>";
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            try {
                var el = engine.Import(SourceFile);
                (el as DOMRoot).Attributes.SetProperty("data-source", SourceFile);
                return el;
            }
            catch (Exception e) {
                throw new PonyTextException($"Import failure: '{SourceFile}'. At {Token}", e);
            }
        }

        public override DocumentFragmentBase DeepClone() {
            return new DocumentInjectionPoint(SourceFile, Token);
        }
    }
}
