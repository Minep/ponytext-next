﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Exceptions;
using PonyTextNext.Api.Runtime;
using System;
using System.Text;

namespace PonyTextNext.Api.Document.Fragments.Functions
{
    public class DocumentFunctionCall : DocumentFragmentBase
    {
        public string Target { get; }
        public DocumentFunctionCall(string fnName, Token token)
            : base(DefaultFragments.Invocation, FragmentLevel.Invocation, token) {
            Target = fnName;
        }

        public override void AddToChildren(DocumentFragmentBase fragmentBase) {
            if (fragmentBase.Name != DefaultFragments.Arguments) {
                throw new InvalidOperationException("A function can only has argument as it's child");
            }
            base.AddToChildren(fragmentBase);
        }

        public override string GetText() {
            var sb = new StringBuilder();

            sb.Append($"<{Name} level=\"{Level}\" target=\"{Target}\">");
            foreach (var child in this.Children()) {
                sb.Append(child.GetText());
            }
            sb.Append($"</{Name}>");

            return sb.ToString();
        }

        public override DOMElement EvaluateToDOM(IEvaluationEngine engine) {
            try {
                var frame = new Runtime.Invocation.CallFrame(this, engine);
                return engine.Invoke(frame);
            }
            catch (Exception e) {
                throw new PonyTextException($"Fail to invoke function '{Target}'", Token, e);
            }
        }

        public override DocumentFragmentBase DeepClone() {
            var obj = new DocumentFunctionCall(Target, Token);
            foreach (var subtree in Children()) {
                obj.AddToChildren(subtree.DeepClone());
            }
            return obj;
        }
    }
}
