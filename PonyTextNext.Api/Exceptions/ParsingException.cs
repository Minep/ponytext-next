﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;

namespace PonyTextNext.Api.Exceptions
{

    [Serializable]
    public class ParsingException : PonyTextException
    {
        public ParsingException() { }
        public ParsingException(string message) : base(message) { }
    }
}
