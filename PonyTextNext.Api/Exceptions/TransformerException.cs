﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;

namespace PonyTextNext.Api.Exceptions
{
    public class TransformerException : PonyTextException
    {
        private string transformerName;
        public TransformerException(string name, string reason) : base(reason) {
            transformerName = name;
        }

        public TransformerException(string name, string reason, Exception innerException) : base(reason, innerException) {
            transformerName = name;
        }

        public override string Message
            => $"Transformer: <{transformerName}> report that \"{base.Message}\"";
    }
}
