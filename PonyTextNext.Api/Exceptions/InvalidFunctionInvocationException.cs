﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Runtime.Invocation;
using System;
using System.Text;

namespace PonyTextNext.Api.Exceptions
{

    [Serializable]
    public class InvalidFunctionInvocationException : PonyTextException
    {
        private CallFrame callFrame;
        public InvalidFunctionInvocationException(CallFrame callFrame, string reason) : base(reason) {
            this.callFrame = callFrame;
        }

        public override string Message
        {
            get {
                var sb = new StringBuilder();
                sb.Append("Fail to evaluate invocation expression: ");
                sb.Append(base.Message);
                return sb.ToString();
            }
        }
    }
}
