﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.Exceptions
{
    public class VarNotDefinedException : PonyTextException
    {
        private string symbolName;

        public VarNotDefinedException(string symbolName) {
            this.symbolName = symbolName;
        }

        public override string Message =>
            $"Symbol '{symbolName}' is neither a mixin, transformer nor variable.";
    }
}
