﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Api.Exceptions
{

    [Serializable]
    public class CyclicDependencyException : PonyTextException
    {
        public List<string> CyclicPath { get; }
        public CyclicDependencyException(List<string> path) {
            CyclicPath = path;
        }

        public override string Message
        {
            get {
                var sb = new StringBuilder();
                sb.Append("A circular dependency is detected on the path: ");
                sb.Append(string.Join(" -> ", CyclicPath));
                return sb.ToString();
            }
        }
    }
}
