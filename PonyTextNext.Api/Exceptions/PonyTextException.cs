﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using System;

namespace PonyTextNext.Api.Exceptions
{

    [Serializable]
    public class PonyTextException : Exception
    {
        private Token token;
        public PonyTextException() { }
        public PonyTextException(string message) : base(message) { }
        public PonyTextException(string message, Token token) : this(message) {
            this.token = token;
        }
        public PonyTextException(string message, Exception inner) : base(message, inner) { }
        public PonyTextException(string message, Token token, Exception inner) : base(message, inner) {
            this.token = token;
        }
        protected PonyTextException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }

        public override string Message {
            get {
                var tokenInfo = token == null ? string.Empty : $"Near {token}";
                return $"{base.Message}. {tokenInfo}";
            }
        }
    }
}
