﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document.Fragments.Terminals;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Config;
using PonyTextNext.Api.Runtime.Registry;
using PonyTextNext.Api.Utilities;

namespace PonyTextNext.Api
{
    public class PonyTextContext
    {
        public string BasePath { get; set; }
        public bool AllowHTML { get; set; } = false;
        public StyleSheet StyleSheet { get; }
        public RegistryBase<DocumentDictionary> TransformerOptions { get; }
        public DocumentAttribute DocumentConfig { get; }
        public HeadingCounter HeadingCounter { get; }
        public HeadingTracker HeadingTracker { get; }

        public PonyTextContext() {
            DocumentConfig = new DocumentAttribute();
            StyleSheet = new StyleSheet();
            TransformerOptions = new GeneralRegistry<DocumentDictionary>();
            HeadingCounter = new HeadingCounter();
            HeadingTracker = new HeadingTracker();
        }
    }
}
