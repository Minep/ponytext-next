﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

namespace PonyTextNext.Api.Utilities
{
    public class SiblingChainer<T> where T : NTreeRoot<T>
    {
        public T Head { get; private set; } = null;

        public SiblingChainer(params T[] elements) {
            if (elements == null || elements.Length == 0) {
                return;
            }

            Head = elements[0];
            for (int i = 1; i < elements.Length; i++) {
                Head.AddToSiblings(elements[i]);
            }
        }

        public void Chain (T sibling) {
            if (Head == null) {
                Head = sibling;
            }
            else {
                Head.AddToSiblings(sibling);
            }
        }
    }
}
