﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Api.Utilities
{
    public class DirectedGraph<NodeType> where NodeType : IComparable
    {
        private Dictionary<NodeType, HashSet<NodeType>> graph;
        private HashSet<NodeType> allNode;

        public DirectedGraph() {
            graph = new Dictionary<NodeType, HashSet<NodeType>>();
            allNode = new HashSet<NodeType>();
        }

        public void PutArrow(NodeType from, NodeType to) {
            if (graph.ContainsKey(from)) {
                graph[from].Add(to);
            }
            else {
                var set = new HashSet<NodeType>();
                set.Add(to);
                graph.Add(from, set);
            }
            allNode.Add(from);
            allNode.Add(to);
        }

        /// <summary>
        /// Get a cyclic path in the graph. If there is not
        /// then returns an empty list
        /// </summary>
        /// <returns></returns>
        public List<NodeType> GetCyclicPathFrom(NodeType startNode) {
            var path = new Stack<NodeType>();
            var accessed = new HashSet<NodeType>();
            var pathElement = new HashSet<NodeType>();
            var node = startNode;
            if (!hasCyclic(node, accessed, path, pathElement)) {
                path.Clear();
            }
            var listPath = new List<NodeType>(path);
            listPath.Reverse();
            return listPath;
        }

        private bool hasCyclic(NodeType currentNode, HashSet<NodeType> visited, Stack<NodeType> path, HashSet<NodeType> pathElement) {
            var adjs = GetAdjcents(currentNode);
            visited.Add(currentNode);

            if (adjs.Count == 0) {
                return false;
            }

            path.Push(currentNode);
            pathElement.Add(currentNode);
            foreach (var node in adjs) {
                if (pathElement.Contains(node)) {
                    path.Push(node);
                    return true;
                }
                if (visited.Contains(node)) {
                    continue;
                }
                if (hasCyclic(node, visited, path, pathElement)) {
                    return true;
                }
            }
            var el = path.Pop();
            pathElement.Remove(el);
            return false;
        }

        private HashSet<NodeType> GetAdjcents(NodeType node) {
            if (graph.ContainsKey(node)) {
                return graph[node];
            }
            return new HashSet<NodeType>();
        }

        private int GetDegree(NodeType node) {
            if (graph.ContainsKey(node)) {
                return graph[node].Count;
            }
            return 0;
        }

        public string ToGraphvizString(NodeType startingVertex) {
            var sb = new StringBuilder();
            var q = new Queue<NodeType>();
            var visited = new HashSet<NodeType>();
            q.Enqueue(startingVertex);
            sb.AppendLine("strict digraph {");
            while (q.Count > 0) {
                var node = q.Dequeue();
                visited.Add(node);
                sb.Append($"\"{node.ToString().Replace("\\", "\\\\")}\" -> {{ ");
                foreach (var adj in GetAdjcents(node)) {
                    if (visited.Contains(adj)) {
                        continue;
                    }
                    q.Enqueue(adj);
                    sb.Append($"\"{adj.ToString().Replace("\\", "\\\\")}\" ");
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");
            return sb.ToString();
        }
    }
}
