﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System;
using System.Collections.Generic;

namespace PonyTextNext.Api.Utilities
{
    public class NTreeRoot<NodeType> where NodeType : NTreeRoot<NodeType>
    {
        public NodeType NextSibling { get; protected set; } = null;

        /// <summary>
        /// Point to the previous node. It the node is the first node,
        /// then this will point to last node.
        /// </summary>
        public NodeType PrevSibling { get; private set; } = null;
        public NodeType DirectSuccessor { get; private set; }

        public bool Head { get; set; } = true;

        public NTreeRoot() {
            // every root is a head when created
            Head = true;
            PrevSibling = (NodeType) this;
        }

        public virtual void AddToSiblings(NodeType newSibling) {
            // all siblings is assumed to be added on head node.
            if (!Head) {
                // if not that case,
                // go forward the list until find a head to add.
                NodeType current = PrevSibling;
                while (current != null && !current.Head) {
                    current = current.PrevSibling;
                }
                if (current != null) {
                    current.AddToSiblings(newSibling);
                    return;
                }
                throw new InvalidOperationException("Can not add to a headless sibling chain");
            }
            else {
                PrevSibling.NextSibling = newSibling;
                if (newSibling.Head) {
                    // if the newSibling is the head of another chain.
                    var originalLast = PrevSibling;
                    PrevSibling = newSibling.PrevSibling;
                    newSibling.PrevSibling = originalLast;
                    newSibling.Head = false;
                }
                else {
                    newSibling.PrevSibling = PrevSibling;
                    PrevSibling = newSibling;
                }
            }
        }

        public virtual void AddToChildren(NodeType newChild) {
            if (DirectSuccessor == null) {
                DirectSuccessor = newChild;
                newChild.Head = true;

                // orphaned
                if (newChild.PrevSibling == null) {
                    newChild.PrevSibling = newChild;
                }
            }
            else {
                DirectSuccessor.AddToSiblings(newChild);
            }
        }

        public void AddToChildrenRanged(IEnumerable<NodeType> children) {
            foreach (var child in children) {
                AddToChildren(child);
            }
        }

        public bool HasChildren
        {
            get {
                return DirectSuccessor != null;
            }
        }

        public virtual IEnumerable<NodeType> Siblings() {
            return chainForEach(NextSibling);
        }

        public virtual IEnumerable<NodeType> Children() {
            return chainForEach(DirectSuccessor);
        }

        public virtual ICollection<NodeType> FlattenToList() {
            var tempList = new LinkedList<NodeType>();
            var current = DirectSuccessor;
            while (current != null) {
                var next = current.NextSibling;
                if (current.DirectSuccessor == null) {
                    current.ToOrphan();
                    tempList.AddLast(current);
                }
                else {
                    foreach (var item in current.FlattenToList()) {
                        tempList.AddLast(item);
                    }
                }
                current = next;
            }
            return tempList;
        }

        public void ReplaceWith(NodeType newNode) {
            var prev = PrevSibling;
            var next = NextSibling;

            newNode.Head = Head;
            newNode.PrevSibling = prev;
            newNode.NextSibling = next;

            if (!Head) {
                prev.NextSibling = newNode;
            }
            if (next != null) {
                next.PrevSibling = newNode;
            }
        }

        public NodeType MoveHeadToNextNonLeaf() {
            if (this.HasChildren || !Head) {
                return (NodeType) this;
            }
            NodeType prev = PrevSibling;    // last of chain
            NodeType current = NextSibling;
            while (current != null && !current.HasChildren) {
                current = current.NextSibling;
            }
            if (current == null) {
                return null;
            }
            current.PrevSibling = prev;
            current.Head = true;
            return current;
        }

        private IEnumerable<NodeType> chainForEach(NodeType startNode) {
            NodeType currentSibling = startNode;
            while (currentSibling != null) {
                yield return currentSibling;
                currentSibling = currentSibling.NextSibling;
            }
        }

        private IEnumerable<NodeType> chainForEachReversed(NodeType startNode) {
            NodeType currentSibling = startNode;
            do {
                yield return currentSibling;
                currentSibling = currentSibling.PrevSibling;
            }
            while (currentSibling != null && currentSibling != startNode);
        }

        /// <summary>
        /// Remove all connections to the siblings of this node.
        /// </summary>
        protected void ToOrphan() {
            this.NextSibling = null;
            this.PrevSibling = null;
            this.Head = false;
        }
    }
}
