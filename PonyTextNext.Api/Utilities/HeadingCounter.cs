﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using System.Collections.Generic;

namespace PonyTextNext.Api.Utilities
{
    public class HeadingCounter
    {
        private int[] counters =
            new int[] { 1, 1, 1, 1, 1, 1 };
        private bool[] scheduledIncrease =
            new bool[] { false, false, false, false, false, false };
        private int initVal = 1;

        /// <summary>
        /// Get or set the starting value of heading numbering.
        /// <para>
        /// Set to a different value will cause the counter 
        /// to reset
        /// </para>
        /// </summary>
        public int InitialValue
        {
            get => initVal;
            set {
                if (value == initVal) {
                    return;
                }
                initVal = value;
                ResetAll();
            }
        }

        public void ResetAll() {
            for (int i = 0; i < counters.Length; i++) {
                counters[i] = InitialValue;
                scheduledIncrease[i] = false;
            }
        }

        public void Count(int level) {
            counters[level]++;
            for (int i = level + 1; i < counters.Length; i++) {
                counters[i] = InitialValue;
                scheduledIncrease[i] = false;
            }
        }

        public IEnumerable<int> EnumerateToLevel(int level) {
            for (int i = 0; i <= level; i++) {
                yield return counters[i];
            }
        }

        public int this[int level]
        {
            get => counters[level];
        }

        public int[] Counters => counters;
    }
}
