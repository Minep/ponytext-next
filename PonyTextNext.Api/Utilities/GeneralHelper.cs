﻿// Copyright (c) Lunaixsky. All rights reserved. 
// Licensed under GPLv3

using PonyTextNext.Api.Document;
using PonyTextNext.Api.DOM;
using PonyTextNext.Api.Runtime;
using PonyTextNext.Api.Runtime.Registry;
using System;
using System.Collections.Generic;
using System.Text;

namespace PonyTextNext.Api.Utilities
{
    public static class GeneralHelper
    {
        public static IEnumerable<Type> GetAsmTypesWithAttribute<T>(this Type referenceType, bool inherited = false)
            where T : Attribute {
            var attrType = typeof(T);
            foreach (var type in referenceType.Assembly.GetTypes()) {
                var attrs = type.GetCustomAttributes(attrType, inherited);
                if (attrs.Length > 0) {
                    yield return type;
                }
            }
        }

        public static bool Indexable (this DocumentFragmentBase fragmentBase) {
            return fragmentBase is IIndexable;
        }

        public static DOMRoot Wrap (this DOMElement dom, string tag) {
            if (!(dom is DOMRoot)) {
                var root = DOMRoot.GetRoot(tag);
                root.AddToChildren(dom);
                return root;
            }
            return (DOMRoot) dom;
        }

        public static void PopulateCssClassesTo(this RegistryBase<KeyValueProperties> classes, StringBuilder builder) {
            foreach (var kvp in classes) {
                builder.Append($"{kvp.Key} {{");
                builder.Append(kvp.Value.ToCss());
                builder.Append("}");
            }
        }
    }
}
